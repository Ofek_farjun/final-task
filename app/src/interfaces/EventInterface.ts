import Item from "./ItemInterface";

export default interface Event extends Item {
    beginningTime: string,
    endingTime: string,
    color: string,
    location?: string | null,
    invitedGuests: string[],
    notificationTime: string,
}

export const isEvent = (item: (Item | null)): item is Event => item !== null && 'color' in item;