import Item from "./ItemInterface";

export default interface Task extends Item {
    estimatedTime: string,
    status: Status,
    priority: Priority,
    review?: string | null,
    timeSpent?: string | null,
    untilDate?: Date | null,
}

export enum Status {
    OPEN = 'Open',
    IN_PROGRESS = 'In Progress',
    CLOSE = 'Close'
}

export enum Priority {
    HIGH = 'High',
    MEDIUM = 'Medium',
    LOW = 'Low',
}

export const isTask = (item: (Item | null)): item is Task => item !== null && 'priority' in item;