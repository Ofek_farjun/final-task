export enum MainContentState {
    TasksAndEvents = 'TasksAndEvents',
    Tasks = 'Tasks',
    Events = 'Events'
}