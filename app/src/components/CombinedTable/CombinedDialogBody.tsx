import React, {useState} from "react";
import TaskDialogBody from "../TasksTable/TaskDialogBody";
import EventDialogBody from "../EventsTable/EventDialogBody";
import {Box, ToggleButton, ToggleButtonGroup} from "@mui/material";
import Task, {isTask} from "../../interfaces/TaskInterface";
import Event from "../../interfaces/EventInterface";
import ConditionalView from "../ConditionalView";
import {makeStyles} from "@mui/styles";
import Item from "../../interfaces/ItemInterface";

const useStyles = makeStyles({
    toggleButtonStyle: {
        textTransform: 'none'
    },
});

export enum CombinedDialogStates {
    TASK = 'TASK',
    EVENT = 'EVENT'
}

interface CombinedDialogBodyProps {
    selectedType: CombinedDialogStates,
    item: Item | null,
    edit: boolean,
    onDone: (data?: Item) => void,
}

const CombinedDialogBody = ({selectedType, item, edit, onDone}: CombinedDialogBodyProps) => {
    const {toggleButtonStyle} = useStyles();
    const [currType, setCurrType] = useState<string>(selectedType);

    return (
        <Box display="flex" flexDirection="column" justifyContent="center" alignItems="center">
            <ConditionalView show={!item}>
                <ToggleButtonGroup
                    color="primary"
                    value={currType}
                    exclusive
                    disabled={item !== null}
                    onChange={(_, val) => val && setCurrType(val)}>
                    <ToggleButton value={CombinedDialogStates.TASK} className={toggleButtonStyle}>
                        Task
                    </ToggleButton>
                    <ToggleButton value={CombinedDialogStates.EVENT} className={toggleButtonStyle}>
                        Event
                    </ToggleButton>
                </ToggleButtonGroup>
            </ConditionalView>
            {
                currType === CombinedDialogStates.TASK ?
                    <TaskDialogBody onDone={onDone} task={item as Task} edit={edit}/>
                    :
                    <EventDialogBody event={item as Event} edit={edit} onDone={onDone}/>
            }
        </Box>
    );
};

export default CombinedDialogBody;