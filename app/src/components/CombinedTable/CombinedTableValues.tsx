import Item from "../../interfaces/ItemInterface";
import { TableHeader } from "../GenericTable/GenericItemsTable";
import { compareDefault, comparePriorities } from "../Utils/TableFilters";

export const COMBINED_TABLE_HEADERS: TableHeader[] = [
    {
        title: 'Type',
        compareFunc: comparePriorities,
    }, {
        title: 'Priority',
        compareFunc: comparePriorities,
    }, {
        title: 'Title',
        compareFunc: (a, b) => compareDefault(a.title, b.title),
    }, {
        title: 'Other',
    }, {
        title: 'Actions',
    }
];