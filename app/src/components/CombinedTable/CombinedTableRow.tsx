import React from "react";
import Divider from "@mui/material/Divider";
import ActionsField from "../ActionsField";
import Task, {isTask} from "../../interfaces/TaskInterface";
import Event from "../../interfaces/EventInterface";
import TypeField from "../TypeField";
import PriorityField from "../TasksTable/PriorityField";
import DoubleRowDisplay from "../DoubleRowDisplay";
import {Stack, Typography} from "@mui/material";
import Item from "../../interfaces/ItemInterface";

interface CombinedTableRowProps {
    item: Item,
    onDisplayItem?: (item: Item) => void,
    onEditItem?: (item: Item) => void,
    onDeleteItem?: (item: Item) => void,
}

const CombinedTableRow = ({item, onDisplayItem, onEditItem, onDeleteItem}: CombinedTableRowProps) => {
    const task = item as Task;
    const event = item as Event;

    return [
        <TypeField item={item}/>,
        <PriorityField item={item}/>,
        <Typography> {item.title} </Typography>,
        <Stack
            direction="row"
            divider={<Divider orientation="vertical" flexItem/>}
            spacing={2}>
            {isTask(item) ?
                <DoubleRowDisplay title={'Status:'}
                                  content={task.status || 'Unknown'}/> : ''}
            {isTask(item) ?
                <DoubleRowDisplay title={'Estimated time:'}
                                  content={task.estimatedTime || 'Unknown'}/> : ''}
            {isTask(item) && task.untilDate ?
                <DoubleRowDisplay title={'Until date:'}
                                  content={new Date(task.untilDate).toLocaleDateString() || 'Unknown'}/> : ''}
            {isTask(item) && task.timeSpent ?
                <DoubleRowDisplay title={'Time spent:'}
                                  content={task.timeSpent || 'Unknown'}/> : ''}
            {!isTask(item) ?
                <DoubleRowDisplay title={'Location:'}
                                  content={event.location || 'Unknown'}/> : ''}
            {!isTask(item) ?
                <DoubleRowDisplay title={'From:'}
                                  content={new Date(event.beginningTime).toLocaleString()}/> : ''}
            {!isTask(item) ?
                <DoubleRowDisplay title={'Until:'}
                                  content={new Date(event.endingTime).toLocaleString()}/> : ''}
        </Stack>,
        <ActionsField
            onDisplay={() => onDisplayItem ? onDisplayItem(item) : null}
            onEdit={() => onEditItem ? onEditItem(item) : null}
            onDelete={() => onDeleteItem ? onDeleteItem(item) : null}
        />
    ]
}

export default CombinedTableRow;