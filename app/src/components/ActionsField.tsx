import React from "react";
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import ViewIcon from '@mui/icons-material/AspectRatio';
import EditIcon from '@mui/icons-material/Edit';

interface ActionsFieldProps {
    onDisplay?: () => void,
    onEdit?: () => void,
    onDelete?: () => void,
}

const ActionsField = ({onDisplay, onDelete, onEdit}: ActionsFieldProps) => (
    <>
        <IconButton aria-label="view"
                    onClick={() => onDisplay && onDisplay()}>
            <ViewIcon/>
        </IconButton>
        <IconButton aria-label="edit" onClick={() => onEdit && onEdit()}>
            <EditIcon/>
        </IconButton>
        <IconButton aria-label="delete"
                    onClick={() => onDelete && onDelete()}>
            <DeleteIcon/>
        </IconButton>
    </>
);

export default ActionsField;