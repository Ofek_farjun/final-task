import React from "react";
import {Dialog, DialogContent} from "@mui/material";

interface GenericDialogProps {
    isShowing: boolean,
    children: React.ReactNode,
}

const GenericDialog = ({ isShowing, children }: GenericDialogProps) =>
    <Dialog open={isShowing}>
        <DialogContent>
            {children}
        </DialogContent>
    </Dialog>

export default GenericDialog;