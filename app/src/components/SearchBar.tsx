import React, { createRef } from "react";
import { TextField } from "@mui/material";

interface SearchBarProps {
    onInputChanged: (filter: string) => void,
}

const SearchBar = ({ onInputChanged }: SearchBarProps) => {
    const inputRef = createRef<HTMLDivElement>();

    document.addEventListener('keydown', (event) => {
        if (event.ctrlKey && event.code === 'KeyF') {
            inputRef.current?.focus();
            event.preventDefault();
        }
    });
    
    return <TextField inputRef={inputRef} label="Search by title" margin="normal" onChange={event => {
        onInputChanged(event.currentTarget.value);
    }} />;
};

export default SearchBar;