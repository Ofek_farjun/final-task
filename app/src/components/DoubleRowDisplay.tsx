import React from "react";
import Box from "@mui/material/Box";
import {Typography} from "@mui/material";

interface DoubleRowDisplay {
    title: string,
    content: string
}

const DoubleRowDisplay = ({ title, content }: DoubleRowDisplay) => (
    <Box display="flex" flexDirection="column">
        <Typography fontWeight="bold">{title}</Typography>
        <Typography>{content}</Typography>
    </Box>
)

export default DoubleRowDisplay;