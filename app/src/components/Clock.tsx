import React, {useEffect, useState} from "react";
import {Typography} from "@mui/material";

const CLOCK_INTERVAL: number = 1000;

const Clock = () => {
    const [time, setTime] = useState<Date>(new Date());

    useEffect(() => {
        const intervalId: NodeJS.Timeout = setInterval(() => setTime(new Date()), CLOCK_INTERVAL);
        return () => clearInterval(intervalId);
    }, []);

    return <Typography variant="h6" fontWeight="bold" marginY="1em">{time.toLocaleString()}</Typography>;
};

export default Clock;