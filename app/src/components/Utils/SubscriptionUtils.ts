export enum MutationType {
    Add = 'ADD',
    Update = 'UPDATE',
    Delete = 'DELETE',
}

export const customQueryMerging = {
    tasks: {
        merge: (existing: unknown, incoming: unknown) => incoming,
      },
      events: {
        merge: (existing: unknown, incoming: unknown) => incoming,
      }
};