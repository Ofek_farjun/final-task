import Item from "../../interfaces/ItemInterface";
import { isTask } from "../../interfaces/TaskInterface";
import { CombinedDialogStates } from "../CombinedTable/CombinedDialogBody";

export interface ContentDialogState<T extends Item> {
    selectedType: CombinedDialogStates,
    selectedItem: T | null,
    editable: boolean
}

function updateDialog<T extends Item>(newState: ContentDialogState<T>, openDialog: (updatedState: ContentDialogState<T>) => void) {
    openDialog(newState);
}

export function dialogContentDisplayItem<T extends Item>(item: T, openDialog: (updatedState: ContentDialogState<T>) => void) {
    updateDialog({
        selectedType: isTask(item) ? CombinedDialogStates.TASK : CombinedDialogStates.EVENT,
        selectedItem: item,
        editable: false,
    }, openDialog);
}

export function dialogContentEditItem<T extends Item>(item: T, openDialog: (updatedState: ContentDialogState<T>) => void) {
    updateDialog({
        selectedType: isTask(item) ? CombinedDialogStates.TASK : CombinedDialogStates.EVENT,
        selectedItem: item,
        editable: true,
    }, openDialog);
}

export function dialogContentCreateItem<T extends Item>(selectedType: CombinedDialogStates, openDialog: (updatedState: ContentDialogState<T>) => void) {
    updateDialog({
        selectedType: selectedType,
        selectedItem: null,
        editable: true,
    }, openDialog);
}

export function dialogContentDeleteItem<T extends Item>(item: T, openDialog: (updatedState: ContentDialogState<T>) => void) {
    updateDialog({
        selectedType: isTask(item) ? CombinedDialogStates.TASK : CombinedDialogStates.EVENT,
        selectedItem: item,
        editable: false,
    }, openDialog);
}