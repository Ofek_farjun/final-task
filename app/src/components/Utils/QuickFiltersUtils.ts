import Event from "../../interfaces/EventInterface";

export const eventsForToday = (events: Event[]) =>
    events.filter(event => new Date(event.beginningTime).toDateString() === (new Date()).toDateString());

export enum QuickFilters {
    TASKS = 'Only tasks',
    EVENTS = 'Only events',
    UNCOMPLETED_TASKS = 'Uncompleted tasks',
    HIGH_PRIORITY_TASKS = 'High priority tasks',
    EVENTS_FOR_TODAY = 'Events for today',
    FUTURE_EVENTS = 'Future events'
}

export const EVENTS_FILTERS: string[] = [QuickFilters.EVENTS_FOR_TODAY, QuickFilters.FUTURE_EVENTS];
export const TASKS_FILTERS: string[] = [QuickFilters.UNCOMPLETED_TASKS, QuickFilters.HIGH_PRIORITY_TASKS];
export const TASKS_AND_EVENTS_FILTERS: string[] = [
    QuickFilters.TASKS,
    QuickFilters.EVENTS,
    QuickFilters.UNCOMPLETED_TASKS,
    QuickFilters.HIGH_PRIORITY_TASKS,
];