import { isEvent } from "../../interfaces/EventInterface";
import Item from "../../interfaces/ItemInterface";
import Task, { isTask, Priority, Status } from "../../interfaces/TaskInterface";

const STATUS_VALUES: Status[] = Object.values(Status);
const PRIORITY_VALUES: Priority[] = Object.values(Priority);

export const compareDefault = (a: string, b: string) => (a < b) ? 1 : -1;

export const compareStatus = (a: Item, b: Item) =>
    (STATUS_VALUES.indexOf(isTask(b) ? b.status : Status.OPEN) - STATUS_VALUES.indexOf(isTask(a) ? a.status : Status.OPEN));

export const comparePriorities = (a: Item, b: Item) =>
    PRIORITY_VALUES.indexOf(isTask(a) ? a.priority : Priority.LOW) - PRIORITY_VALUES.indexOf(isTask(b) ? b.priority : Priority.LOW);

const calcEstimatedTimeValue = (estimatedStr: string) => {
    let sum: number = 0;
    const portions: string[] = estimatedStr.split(' ');
    portions.forEach(part => {
        if (part.includes('s'))
            sum += parseFloat(part.replace('s', ''));
        if (part.includes('m'))
            sum += parseFloat(part.replace('m', '')) * 60;
        if (part.includes('h'))
            sum += parseFloat(part.replace('h', '')) * 60 * 60;
        if (part.includes('d'))
            sum += parseFloat(part.replace('d', '')) * 24 * 60 * 60;
        if (part.includes('w'))
            sum += parseFloat(part.replace('w', '')) * 7 * 24 * 60 * 60;
        if (part.includes('M'))
            sum += parseFloat(part.replace('M', '')) * 31 * 24 * 60 * 60;
        if (part.includes('y'))
            sum += parseFloat(part.replace('y', '')) * 365.5 * 24 * 60 * 60;
    });

    return sum;
}

export const compareEstimatedTime = (a: Item, b: Item) =>
    calcEstimatedTimeValue(isTask(a) ? a.estimatedTime : new Date().toISOString())
    -
    calcEstimatedTimeValue(isTask(b) ? b.estimatedTime : new Date().toISOString());

export const compareBeginningTime = (a: Item, b: Item) =>
    compareDefault(isEvent(a) ? a.beginningTime : new Date().toISOString(), isEvent(b) ? b.beginningTime : new Date().toISOString())

export const compareEndingTime = (a: Item, b: Item) =>
    compareDefault(isEvent(a) ? a.endingTime : new Date().toISOString(), isEvent(b) ? b.endingTime : new Date().toISOString())

export const compareColor = (a: Item, b: Item) =>
    compareDefault(isEvent(a) ? a.color : '', isEvent(b) ? b.color : '')

export const compareLocation = (a: Item, b: Item) =>
    compareDefault(isEvent(a) ? a.location || '' : '', isEvent(b) ? b.location || '' : '')
