const Validations = {
    requiredString: (input?: string) => input && (input.length > 0),
    requiredDate: (dateStr?: string) => dateStr && new Date(dateStr).toString() !== 'Invalid Date',
    includedInArray: (values: any[], desiredVal: any) => values.indexOf(desiredVal) !== -1,
    isEstimatedTimeValid: (input: string) => input.replaceAll(' ', '').length > 0 &&
        /^([0-9]*y)? ?([0-9]*M)? ?([0-9]*w)? ?([0-9]*d)? ?(([0-1]?[0-9]|2[0-3])h)? ?([0-5]?[0-9]m)? ?([0-5]?[0-9]s)?$/g.test(input),
}

export default Validations;