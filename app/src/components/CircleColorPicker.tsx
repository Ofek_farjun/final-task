import React, {useState} from "react";
import {CirclePicker} from "react-color";
import {makeStyles} from "@mui/styles";
import {Typography} from "@mui/material";

const useStyles = makeStyles({
    colorPaletteStyle: {
        alignSelf: 'center'
    }
});

interface CircleColorPickerProps {
    defaultColor: string,
    disabled: boolean,
    onColorSelected: (color: string) => void,
}

const CircleColorPicker = ({defaultColor, onColorSelected, disabled}: CircleColorPickerProps) => {
    const {colorPaletteStyle} = useStyles();
    const [currColor, setCurrColor] = useState<string>(defaultColor);

    return <>
        <Typography marginY="1em">Color</Typography>
        <CirclePicker color={currColor} onChange={({hex}: { hex: string }) => {
            if (!disabled) {
                setCurrColor(hex);
                onColorSelected(hex);
            }
        }}
                      className={colorPaletteStyle}/>
    </>;
}

export default CircleColorPicker;