import React from "react";
import Circle from '@mui/icons-material/FiberManualRecord';
import Event from "../../interfaces/EventInterface";

interface EventColorFieldProps {
    event: Event
}

const EventColorField = ({event}: EventColorFieldProps) => <Circle htmlColor={event.color}/>;

export default EventColorField;