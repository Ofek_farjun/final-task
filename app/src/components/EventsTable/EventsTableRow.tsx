import React from "react";
import Event, { isEvent } from "../../interfaces/EventInterface";
import ActionsField from "../ActionsField";
import EventColorField from "./EventColorField";
import {Typography} from "@mui/material";
import Item from "../../interfaces/ItemInterface";

interface EventsTableRowProps {
    item: Item,
    onRowDoubleClick?: () => void,
    onDisplayItem: (event: Event) => void,
    onEditItem: (event: Event) => void,
    onDeleteItem: (event: Event) => void,
}

const EventsTableRow = ({item, onEditItem, onDisplayItem, onDeleteItem}: EventsTableRowProps) => isEvent(item) ? [
    <EventColorField event={item}/>,
    <Typography> {item.title} </Typography>,
    <Typography> {new Date(item.beginningTime).toLocaleString()} </Typography>,
    <Typography> {new Date(item.endingTime).toLocaleString()} </Typography>,
    <Typography> {item.location || '-'} </Typography>,
    <ActionsField
        onDisplay={() => onDisplayItem(item)}
        onEdit={() => onEditItem(item)}
        onDelete={() => onDeleteItem(item)}
    />
] : [];


export default EventsTableRow;