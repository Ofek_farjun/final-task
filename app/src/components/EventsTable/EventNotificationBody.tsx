import React from "react";
import Event from '../../interfaces/EventInterface';
import {Box, Typography} from "@mui/material";
import LocationOnIcon from '@mui/icons-material/LocationOn';
import PeopleIcon from '@mui/icons-material/People';

interface EventNotificationBodyProps {
    event: Event,
}

const EventNotificationBody = ({event}: EventNotificationBodyProps) => (
    <Box bgcolor={event.color} padding="1em" borderRadius="1em">
        <Box display="flex" alignItems="center">
            <Typography variant="h5" fontWeight="bold" marginY="1em">
                {event.title}
            </Typography>
            <Box display="flex" flexDirection="column" textAlign="center">
                <Typography>{event.beginningTime.toLocaleString()}</Typography>
                <Typography><LocationOnIcon/>{event.location || 'Unknown'}</Typography>
                <Typography>{event.invitedGuests.length}<PeopleIcon/></Typography>
            </Box>
        </Box>
    </Box>
);

export default EventNotificationBody;