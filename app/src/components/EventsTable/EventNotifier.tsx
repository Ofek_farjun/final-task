import React, { useEffect, useState } from "react";
import Event from '../../interfaces/EventInterface';
import SnackbarAlert from "../SnackbarAlert";
import EventNotificationBody from "./EventNotificationBody";

interface EventNorifierProps {
    events: Event[],
}

const EventNotifier = ({ events }: EventNorifierProps) => {
    const [currentEvent, setCurrentEvent] = useState<Event>(events[0]);
    const [snackbarShowing, setSnackbarShowing] = useState<boolean>(false);

    useEffect(() => {
        const timeouts: NodeJS.Timeout[] = [];
        events.forEach(event => {
            const duration: number = new Date(event.notificationTime).getTime() - (new Date()).getTime();
            if (duration > 0) {
                const instance: NodeJS.Timeout = setTimeout(() => {
                    setCurrentEvent(event);
                    setSnackbarShowing(true);
                }, duration);
                timeouts.push(instance);
            }
        });

        return () => timeouts.forEach(instance => clearTimeout(instance));
    }, [events]);

    return (
        <SnackbarAlert isShowing={snackbarShowing} onClose={() => setSnackbarShowing(false)}>
            <EventNotificationBody event={currentEvent} />
        </SnackbarAlert>
    );
};

export default EventNotifier;