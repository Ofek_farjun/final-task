import React, {useState} from "react";
import {Autocomplete, Box, Button, Grid, TextField, Typography} from "@mui/material";
import Event from "../../interfaces/EventInterface";
import moment from "moment";
import {uuidv4} from "../Utils/Utils";
import CircleColorPicker from "../CircleColorPicker";
import Validations from "../Utils/Validations";

interface EventDialogBodyProps {
    event: Event | null,
    edit: boolean,
    onDone: (data?: Event) => void,
}

interface EventDialogInputData {
    title: string,
    description: string,
    beginningTime: string,
    endingTime: string,
    color: string,
    location?: string,
    invitedGuests: string[],
    notificationTime: string,
}

const EventDialogBody = ({event, edit, onDone}: EventDialogBodyProps) => {
    const [showErrors, setShowError] = useState<boolean>(false);
    const [inputData, setInputData] = useState<EventDialogInputData>({
        title: event?.title || '',
        description: event?.description || '',
        beginningTime: toDatePickerFormat(event?.beginningTime ? new Date(event.beginningTime) : new Date()),
        endingTime: toDatePickerFormat(event?.endingTime ? new Date(event.endingTime) : new Date()),
        color: event?.color || '#3f51b5',
        location: event?.location || undefined,
        invitedGuests: event?.invitedGuests || [],
        notificationTime: toTimePickerFormat(event?.notificationTime ? new Date(event.notificationTime) : new Date()),
    });

    const setTitle = (title: string) => setInputData({...inputData, title});
    const setDescription = (description: string) => setInputData({...inputData, description});
    const setBeginningTime = (beginningTimeStr: string) => setInputData({
        ...inputData,
        beginningTime: beginningTimeStr
    });
    const setEndingTime = (endingTimeStr: string) => setInputData({...inputData, endingTime: endingTimeStr});
    const setColor = (color: string) => setInputData({...inputData, color});
    const setLocation = (location: string) => setInputData({...inputData, location});
    const setInvitedGuests = (invitedGuests: string[]) => setInputData({...inputData, invitedGuests});
    const setNotificationTime = (notificationTimeStr: string) => setInputData({
        ...inputData,
        notificationTime: notificationTimeStr
    });

    const onSave = () => {
        if (isInputDataValid()) {
            onDone({
                id: event?.id || uuidv4(),
                title: inputData.title,
                description: inputData.description,
                beginningTime: new Date(inputData.beginningTime).toISOString(),
                endingTime: new Date(inputData.endingTime).toISOString(),
                color: inputData.color,
                location: inputData.location,
                invitedGuests: inputData.invitedGuests,
                notificationTime: calcNotificationTime(inputData.notificationTime, new Date(inputData.beginningTime)).toISOString(),
            });
        } else {
            setShowError(true);
        }
    }

    const isInputDataValid = () => Validations.requiredString(inputData.title) &&
        Validations.requiredString(inputData.description) &&
        Validations.requiredString(inputData.color) &&
        Validations.requiredDate(inputData.beginningTime) &&
        Validations.requiredDate(inputData.endingTime);

    const onClose = () => onDone();

    return (
        <Grid container>
            <Grid item xs={12}>
                <Typography variant="h5" fontWeight="bold" marginY="1em">
                    {event ? 'Event' : 'New Event'}
                </Typography>
            </Grid>

            <Grid item xs={6}>
                <TextField label="Title*" defaultValue={inputData.title} margin="normal"
                           placeholder="Insert your title here..."
                           disabled={!edit}
                           fullWidth
                           error={showErrors && !Validations.requiredString(inputData.title)}
                           onChange={({currentTarget}) => setTitle(currentTarget.value)}/>
            </Grid>
            <Grid item xs={6}>
                <TextField label="Location" defaultValue={inputData.location} margin="normal"
                           placeholder="Insert your location here..."
                           disabled={!edit}
                           fullWidth
                           onChange={({currentTarget}) => setLocation(currentTarget.value)}/>
            </Grid>

            <Grid item xs={12}>
                <TextField label="Description*" defaultValue={inputData.description} margin="normal"
                           placeholder="Insert your description here..." multiline rows={4}
                           fullWidth
                           disabled={!edit}
                           error={showErrors && !Validations.requiredString(inputData.description)}
                           onChange={({currentTarget}) => setDescription(currentTarget.value)}/>
            </Grid>

            <Grid item xs={6}>
                <TextField label="Beginning Time*" defaultValue={inputData.beginningTime}
                           margin="normal"
                           type="datetime-local"
                           disabled={!edit}
                           fullWidth
                           InputLabelProps={{shrink: true}}
                           error={showErrors && !Validations.requiredDate(inputData.beginningTime)}
                           onChange={({currentTarget}) => setBeginningTime(currentTarget.value)}/>
            </Grid>
            <Grid item xs={6}>
                <TextField label="Ending Time*" defaultValue={inputData.endingTime}
                           margin="normal"
                           type="datetime-local"
                           fullWidth
                           disabled={!edit}
                           InputLabelProps={{shrink: true}}
                           error={showErrors && !Validations.requiredDate(inputData.endingTime)}
                           onChange={({currentTarget}) => setEndingTime(currentTarget.value)}/>
            </Grid>

            <Grid item xs={12} display="flex" flexDirection="column" justifyContent="center">
                <CircleColorPicker defaultColor={inputData.color} onColorSelected={setColor} disabled={!edit}/>
            </Grid>

            <Grid item xs={6}>
                <Autocomplete
                    disablePortal
                    multiple
                    freeSolo
                    options={[]}
                    value={inputData.invitedGuests}
                    disabled={!edit}
                    renderInput={(params) => <TextField {...params} margin="normal"
                                                        label="Invited guests*"
                                                        placeholder="Insert a guest..."/>}
                    onChange={(_, value) => setInvitedGuests(value as string[])}
                />
            </Grid>
            <Grid item xs={6}>
                <TextField label="Notification time*"
                           defaultValue={inputData.notificationTime} margin="normal"
                           type="time"
                           disabled={!edit}
                           fullWidth
                           InputLabelProps={{shrink: true}}
                           onChange={({currentTarget}) => setNotificationTime(currentTarget.value)}/>
            </Grid>

            <Box display="flex" justifyContent="space-between" width="100%">
                <Button onClick={() => onClose()}>Close</Button>
                <Button onClick={() => onSave()} disabled={!edit}>Save</Button>
            </Box>
        </Grid>
    );
}

const toDatePickerFormat = (date: Date) => moment(date).format(`yyyy-MM-DDThh:mm`);
const toTimePickerFormat = (date: Date) => moment(date).format(`HH:mm`);
const fromTimePickerFormat = (str: string) => moment(str, `HH:mm`).toDate();

const calcNotificationTime = (notificationTimeStr: string, beginningTime: Date) => {
    const notificationTime = new Date(beginningTime);
    const desiredTime = fromTimePickerFormat(notificationTimeStr);
    notificationTime.setHours(desiredTime.getHours());
    notificationTime.setMinutes(desiredTime.getMinutes());
    notificationTime.setSeconds(desiredTime.getSeconds());
    return notificationTime;
}

export default EventDialogBody;