import { compareBeginningTime, compareColor, compareDefault, compareEndingTime, compareLocation } from "../Utils/TableFilters";
import { TableHeader } from "../GenericTable/GenericItemsTable";

export const EVENTS_TABLE_HEADERS: TableHeader[] = [
    {
        title: 'Color',
        compareFunc: compareColor,
    }, {
        title: 'Title',
        compareFunc: (a, b) => compareDefault(a.title, b.title),
    }, {
        title: 'From',
        compareFunc: compareBeginningTime,
    }, {
        title: 'Until',
        compareFunc: compareEndingTime,
    }, {
        title: 'Location',
        compareFunc: compareLocation,
    }, {
        title: 'Actions',
    }
];