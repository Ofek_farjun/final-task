import React from "react";
import { Box, ToggleButton, ToggleButtonGroup } from "@mui/material";
import { makeStyles } from "@mui/styles";
import SearchBar from "./SearchBar";
import { useDispatch, useSelector } from "react-redux";
import { filtersSelector } from "../store/filters/selectors/FiltersSelectors";
import { setSearchFilterAction, setSelectedQuickFiltersAction } from "../store/filters/actions/FiltersActions";
import { FiltersState } from "../store/filters/FiltersStore";
import { QuickFilters } from "./Utils/QuickFiltersUtils";

const useStyles = makeStyles({
    titleStyle: {
        margin: '1em',
    },
    toggleButtonStyle: {
        textTransform: 'none !important' as 'none',
    },
});

export interface QuickFiltersRowProps {
    filters: string[],
}

const QuickFiltersRow = ({ filters }: QuickFiltersRowProps) => {
    const dispatch = useDispatch();
    const { titleStyle, toggleButtonStyle } = useStyles();
    const filtersState = useSelector(filtersSelector);

    const setSelectedFilters = (filters: string[]) =>
        dispatch(setSelectedQuickFiltersAction(toFiltersState(filters)));
    const setSearchFilter = (searchFilter: string) => dispatch(setSearchFilterAction(searchFilter));

    return (
        <Box display="flex" justifyContent="space-between" alignItems="center">
            <SearchBar onInputChanged={setSearchFilter} />
            <div>
                <span className={titleStyle}>Quick Filters: </span>
                <ToggleButtonGroup
                    color="primary"
                    value={fromFiltersState(filtersState)}
                    onChange={(_, val) => setSelectedFilters(val)}>
                    {
                        filters.map((filter, index) =>
                            <ToggleButton value={filter} key={index} className={toggleButtonStyle}>
                                {filter}
                            </ToggleButton>)
                    }
                </ToggleButtonGroup>
            </div>
        </Box>
    );
};

const toFiltersState = (selectedFilters: string[]): FiltersState => ({
    searchFilter: '',
    onlyTasks: selectedFilters.indexOf(QuickFilters.TASKS) !== -1,
    onlyEvents: selectedFilters.indexOf(QuickFilters.EVENTS) !== -1,
    uncompletedTasks: selectedFilters.indexOf(QuickFilters.UNCOMPLETED_TASKS) !== -1,
    highPriorityTasks: selectedFilters.indexOf(QuickFilters.HIGH_PRIORITY_TASKS) !== -1,
    eventsForToday: selectedFilters.indexOf(QuickFilters.EVENTS_FOR_TODAY) !== -1,
    futureEvents: selectedFilters.indexOf(QuickFilters.FUTURE_EVENTS) !== -1,
});

const fromFiltersState = (filters: FiltersState): string[] => {
    const selectedFilters: string[] = [];

    filters.onlyTasks && selectedFilters.push(QuickFilters.TASKS);
    filters.onlyEvents && selectedFilters.push(QuickFilters.EVENTS);
    filters.eventsForToday && selectedFilters.push(QuickFilters.EVENTS_FOR_TODAY);
    filters.futureEvents && selectedFilters.push(QuickFilters.FUTURE_EVENTS);
    filters.uncompletedTasks && selectedFilters.push(QuickFilters.UNCOMPLETED_TASKS);
    filters.highPriorityTasks && selectedFilters.push(QuickFilters.HIGH_PRIORITY_TASKS);

    return selectedFilters;
}

export default QuickFiltersRow;