import React, { useState } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableHead from '@mui/material/TableHead';
import Paper from '@mui/material/Paper';
import { Box, Button, TableCell, TableRow } from "@mui/material";
import GenericTableHeader from "./GenericTableHeader";
import Item from "../../interfaces/ItemInterface";
import CombinedTableRow from "../CombinedTable/CombinedTableRow";
import TasksTableRow from "../TasksTable/TaskTableRow";
import EventsTableRow from "../EventsTable/EventsTableRow";
import { COMBINED_TABLE_HEADERS } from "../CombinedTable/CombinedTableValues";
import { EVENTS_TABLE_HEADERS } from "../EventsTable/EventsTableValues";
import { TASKS_TABLE_HEADERS } from "../TasksTable/TasksTableValues";
import { MainContentState } from "../../interfaces/MainContentState";
import { useSelector } from "react-redux";
import QuickFiltersRow from "../QuickFiltersRow";
import { ContentDialogState, dialogContentCreateItem, dialogContentDeleteItem, dialogContentDisplayItem, dialogContentEditItem } from "../Utils/ContentDialogUtils";
import { TASKS_FILTERS, EVENTS_FILTERS, TASKS_AND_EVENTS_FILTERS } from "../Utils/QuickFiltersUtils";
import { isEvent } from "../../interfaces/EventInterface";
import CombinedDialogBody, { CombinedDialogStates } from "../CombinedTable/CombinedDialogBody";
import ConfirmationDialog from "../ConfirmationDialog";
import useDialogContext from "../../hooks/UseDialogContext";
import { useSubscription } from "@apollo/client";
import { filtersSelector } from "../../store/filters/selectors/FiltersSelectors";
import { TaskSubscriptionResult, TASK_SUBSCRIPTION } from "../../graphql/tasks/subscriptions/TasksSubscriptions";
import EventNotifier from "../EventsTable/EventNotifier";
import { useItems } from "../../hooks/UseItems";

interface ItemTableRowContentAdditionalProps {
    onDisplayItem: (item: Item) => void,
    onEditItem: (item: Item) => void,
    onDeleteItem: (item: Item) => void
}

interface ItemTableRowContentProps extends ItemTableRowContentAdditionalProps {
    item: Item
}

export interface TableHeader {
    title: string,
    compareFunc?: (a: Item, b: Item) => number,
}

interface TableContentType {
    headers: TableHeader[],
    rowContent: (props: ItemTableRowContentProps) => JSX.Element[],
}

const TABLE_CONTENTS = new Map<MainContentState, TableContentType>([
    [MainContentState.TasksAndEvents, {
        headers: COMBINED_TABLE_HEADERS,
        rowContent: CombinedTableRow,
    }],
    [MainContentState.Tasks, {
        headers: TASKS_TABLE_HEADERS,
        rowContent: TasksTableRow,
    }],
    [MainContentState.Events, {
        headers: EVENTS_TABLE_HEADERS,
        rowContent: EventsTableRow,
    }]
]);

interface GenericItemsTableProps {
    tableType: MainContentState,
}

const QUICK_FILTERS: Map<MainContentState, string[]> = new Map([
    [MainContentState.Tasks, TASKS_FILTERS],
    [MainContentState.Events, EVENTS_FILTERS],
    [MainContentState.TasksAndEvents, TASKS_AND_EVENTS_FILTERS],
]);

export default function GenericItemsTable({
    tableType,
}: GenericItemsTableProps) {
    const [orderedTitle, setOrderedTitle] = useState<string>('');
    const [orderDir, setOrderDir] = useState<'asc' | 'desc'>('asc');
    const tableContent = TABLE_CONTENTS.get(tableType)!;
    const { openDialog, closeDialog } = useDialogContext();

    const {
        items,
        createItem,
        updateItem,
        deleteItem
    } = useItems();

    const selectedItemType = () =>
        tableType === MainContentState.Events ? CombinedDialogStates.EVENT : CombinedDialogStates.TASK;


    const showCombinedDialog = ({ selectedItem, selectedType, editable }: ContentDialogState<Item>) => openDialog(
        <CombinedDialogBody
            item={selectedItem}
            edit={editable}
            selectedType={selectedType}
            onDone={(item) => {
                closeDialog();
                onDialogDone(selectedItem, item);
            }}
        />);

    const onDialogDone = (selectedItem: Item | null, item?: Item) =>
        item && (selectedItem ? updateItem(item) : createItem(item));

    const showConfirmationDialog = ({ selectedItem }: ContentDialogState<Item>) => openDialog(
        <ConfirmationDialog title={'Are you sure you want to delete this item?'}
            onDecision={(decision) => {
                closeDialog();
                decision && deleteItem(selectedItem!);
            }} />);

    const rowContentProps: ItemTableRowContentAdditionalProps = {
        onDisplayItem: (item) => dialogContentDisplayItem(item, showCombinedDialog),
        onEditItem: (item) => dialogContentEditItem(item, showCombinedDialog),
        onDeleteItem: (item) => dialogContentDeleteItem(item, showConfirmationDialog),
    }

    const orderedHeader = tableContent.headers.find(header => header.title === orderedTitle);

    const sortFunc = (a: Item, b: Item): number => {
        if (orderedHeader?.compareFunc) {
            return orderedHeader?.compareFunc(a, b) * (orderDir === 'asc' ? 1 : -1);
        }
        return 0;
    }

    const onHeaderClick = (header: TableHeader) => {
        if (header.title !== orderedTitle) {
            setOrderedTitle(header.title);
            setOrderDir('asc');
        } else {
            setOrderDir(orderDir === 'asc' ? 'desc' : 'asc');
        }
    }

    const handleRowClick = (event: React.MouseEvent, item: Item) =>
        event.detail === 2 && dialogContentDisplayItem(item, showCombinedDialog);

    return <Box flexGrow={1} height="100%" display="flex" flexDirection="column" overflow="hidden"
        padding="1em">
        <QuickFiltersRow filters={QUICK_FILTERS.get(tableType) || TASKS_AND_EVENTS_FILTERS} />
        <Box flexGrow={1} overflow="auto">
            <Box height="100%" display="flex" flexDirection="column">
                <Box flexGrow={1} overflow="auto">
                    <Paper>
                        <Table size="small" stickyHeader>
                            <TableHead>
                                <GenericTableHeader headers={tableContent.headers} orderedHeaderTitle={orderedTitle}
                                    orderDir={orderDir} onHeaderClick={onHeaderClick} />
                            </TableHead>
                            <TableBody>
                                {
                                    items.sort(sortFunc).map(item =>
                                        <TableRow key={item.id} onClick={(e) => handleRowClick(e, item)}>
                                            {
                                                tableContent.rowContent({ item, ...rowContentProps }).map((CellContent, index) =>
                                                    <TableCell align="center" key={index}>
                                                        {CellContent}
                                                    </TableCell>
                                                )
                                            }
                                        </TableRow>
                                    )
                                }
                            </TableBody>
                        </Table>
                    </Paper>
                </Box>
                <Button onClick={() => dialogContentCreateItem(selectedItemType(), showCombinedDialog)}>Create a new item</Button>
                <EventNotifier events={items.filter(isEvent)} />
            </Box>
        </Box>
    </Box>
}