import React from "react";
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import { TableSortLabel } from "@mui/material";
import { TableHeader } from "./GenericItemsTable";

interface GenericTableHeaderProps {
    headers: TableHeader[],
    orderedHeaderTitle: string,
    orderDir: 'asc' | 'desc',
    onHeaderClick: (header: TableHeader) => void,
}

const GenericTableHeader = ({
    headers,
    orderedHeaderTitle,
    orderDir,
    onHeaderClick
}: GenericTableHeaderProps) => (
    <TableRow>
        {
            headers.map((header, index) =>
                <TableCell
                    align="center"
                    key={index}>
                    <TableSortLabel
                        active={header.title === orderedHeaderTitle}
                        direction={orderDir}
                        disabled={!(header.compareFunc)}
                        onClick={() => onHeaderClick(header)}>
                        {header.title}
                    </TableSortLabel>
                </TableCell>)
        }
    </TableRow>
);

export default GenericTableHeader;