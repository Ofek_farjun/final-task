import React from "react";
import Circle from '@mui/icons-material/FiberManualRecord';
import Task, { isTask, Priority } from "../../interfaces/TaskInterface";
import Item from "../../interfaces/ItemInterface";

interface PriorityFieldProps {
    item: Item
}

const PriorityField = ({ item }: PriorityFieldProps) => {
    if (isTask(item)) {
        switch ((item as Task).priority) {
            case Priority.LOW:
                return <Circle htmlColor="green" />;
            case Priority.MEDIUM:
                return <Circle htmlColor="orange" />;
            case Priority.HIGH:
                return <Circle htmlColor="red" />;
            default:
                return <>Unknown</>;
        }
    } else {
        return <Circle htmlColor="gray" />
    }
}

export default PriorityField;