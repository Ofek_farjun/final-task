import React, {useState} from "react";
import {Autocomplete, Box, Button, Grid, TextField, Typography} from "@mui/material";
import Task, {Priority, Status} from "../../interfaces/TaskInterface";
import {uuidv4} from "../Utils/Utils";
import Validations from "../Utils/Validations";

interface TaskDialogBodyProps {
    task: Task | null,
    edit: boolean,
    onDone: (data?: Task) => void,
}

interface TaskDialogInputData {
    title: string,
    description: string,
    estimatedTime: string,
    status: Status,
    priority: Priority,
    review?: string,
    timeSpent?: string,
    untilDate?: Date,
}

const STATUS_OPTIONS = Object.values(Status);
const PRIORITY_OPTIONS = Object.values(Priority);

const TaskDialogBody = ({task, edit, onDone}: TaskDialogBodyProps) => {
    const [showErrors, setShowErrors] = useState(false);
    const [inputData, setInputData] = useState<TaskDialogInputData>({
        title: task?.title || '',
        description: task?.description || '',
        estimatedTime: task?.estimatedTime || '',
        status: task?.status || Status.OPEN,
        priority: task?.priority || Priority.LOW,
        review: task?.review || undefined,
        timeSpent: task?.timeSpent || undefined,
        untilDate: task?.untilDate || undefined,
    });

    const setTitle = (title: string) => setInputData({...inputData, title});
    const setDescription = (description: string) => setInputData({...inputData, description});
    const setEstimatedTime = (estimatedTime: string) => setInputData({...inputData, estimatedTime: estimatedTime});
    const setStatus = (status: Status | null) => status && setInputData({...inputData, status});
    const setPriority = (priority: Priority | null) => priority && setInputData({...inputData, priority});

    const onSave = () => isInputDataValid() ? onDone({
        id: task?.id || uuidv4(),
        title: inputData.title,
        description: inputData.description,
        estimatedTime: inputData.estimatedTime,
        status: inputData.status,
        priority: inputData.priority,
        review: inputData.review,
        timeSpent: inputData.timeSpent,
        untilDate: inputData.untilDate,
    }) : setShowErrors(true);

    const onClose = () => onDone();

    const isInputDataValid = () => Validations.requiredString(inputData.title) &&
        Validations.requiredString(inputData.description) &&
        Validations.includedInArray(Object.values(Status), inputData.status) &&
        Validations.includedInArray(Object.values(Priority), inputData.priority) &&
        Validations.isEstimatedTimeValid(inputData.estimatedTime);

    return (
        <Grid container>
            <Grid item xs={12}>
                <Typography variant="h5" fontWeight="bold" marginY="1em">
                    {task ? 'Task' : 'New Task'}
                </Typography>
            </Grid>

            <Grid item xs={6}>
                <TextField label="Title*" defaultValue={inputData.title} margin="normal"
                           placeholder="Insert your title here..."
                           disabled={!edit}
                           fullWidth
                           error={showErrors && !Validations.requiredString(inputData.title)}
                           onChange={({currentTarget}) => setTitle(currentTarget.value)}/>
            </Grid>
            <Grid item xs={6}>
                <Autocomplete
                    disablePortal
                    options={STATUS_OPTIONS}
                    disabled={!edit}
                    renderInput={(params) => <TextField {...params} margin="normal"
                                                        error={showErrors && !Validations.includedInArray(Object.values(Status), inputData.status)}
                                                        label="Status"/>}
                    value={inputData.status}
                    onChange={(_, value) => setStatus(value)}
                />
            </Grid>

            <Grid item xs={12}>
                <TextField label="Description*" defaultValue={inputData.description} margin="normal"
                           placeholder="Insert your description here..." multiline rows={4}
                           fullWidth
                           disabled={!edit}
                           error={showErrors && !Validations.requiredString(inputData.description)}
                           onChange={({currentTarget}) => setDescription(currentTarget.value)}/>
            </Grid>

            <Grid item xs={6}>
            <TextField label="Estimated time*" defaultValue={inputData.estimatedTime} margin="normal"
                       placeholder="*y *M *w *d *h *m *s"
                       disabled={!edit}
                       fullWidth
                       error={showErrors && !Validations.isEstimatedTimeValid(inputData.estimatedTime)}
                       helperText="*y *M *w *d *h *m *s"
                       onChange={({currentTarget}) => setEstimatedTime(currentTarget.value)}/>
            </Grid>
            <Grid item xs={6}>
                <Autocomplete
                    disablePortal
                    options={PRIORITY_OPTIONS}
                    disabled={!edit}
                    renderInput={(params) => <TextField {...params} margin="normal"
                                                        error={showErrors && !Validations.includedInArray(Object.values(Priority), inputData.priority)}
                                                        label="Priority"/>}
                    value={inputData.priority}
                    onChange={(_, value) => setPriority(value)}
                />
            </Grid>

            <Box display="flex" justifyContent="space-between" width="100%">
                <Button onClick={() => onClose()}>Close</Button>
                <Button onClick={() => onSave()} disabled={!edit}>Save</Button>
            </Box>
        </Grid>
    );
}

export default TaskDialogBody;