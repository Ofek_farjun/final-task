import React from "react";
import Task, {isTask, Status} from "../../interfaces/TaskInterface";
import DoubleRowDisplay from "../DoubleRowDisplay";
import TypeField from "../TypeField";
import PriorityField from "./PriorityField";
import ActionsField from "../ActionsField";
import {Box, Typography} from "@mui/material";
import Item from "../../interfaces/ItemInterface";

interface TasksTableCellProps {
    item: Item,
    onDisplayItem: (task: Task) => void,
    onEditItem: (task: Task) => void,
    onDeleteItem: (task: Task) => void,
}

const TasksTableRow = ({item, onDisplayItem, onEditItem, onDeleteItem}: TasksTableCellProps) => isTask(item) ? [
    <TypeField item={item}/>,
    <PriorityField item={item}/>,
    <Typography> {item.title} </Typography>,
    <Typography> {item.status} </Typography>,
    <Typography> {item.estimatedTime} </Typography>,
    <Box>
        {
            item.status === Status.IN_PROGRESS ?
                <DoubleRowDisplay title={'UntilDate:'}
                                  content={item.untilDate ? new Date(item.untilDate).toLocaleDateString() : 'Unknown'}/> : ''
        }
        {
            item.status === Status.CLOSE ?
                <DoubleRowDisplay title={'TimeSpent:'} content={item.timeSpent || 'Unknown'}/> : ''
        }
    </Box>,
    <ActionsField
        onDisplay={() => onDisplayItem(item)}
        onEdit={() => onEditItem(item)}
        onDelete={() => onDeleteItem(item)}
    />
] : [];


export default TasksTableRow;