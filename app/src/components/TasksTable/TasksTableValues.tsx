import { TableHeader } from "../GenericTable/GenericItemsTable";
import { compareEstimatedTime, comparePriorities, compareStatus, compareDefault } from "../Utils/TableFilters";

export const TASKS_TABLE_HEADERS: TableHeader[] = [
    {
        title: 'Type',
        compareFunc: compareStatus,
    },
    {
        title: 'Priority',
        compareFunc: comparePriorities,
    },
    {
        title: 'Title',
        compareFunc: (a, b) => compareDefault(a.title, b.title),
    },
    {
        title: 'Status',
        compareFunc: compareStatus,
    },
    {
        title: 'Estimated Time',
        compareFunc: compareEstimatedTime,
    },
    {
        title: 'Other',
    },
    {
        title: 'Actions',
    }
];