import React from "react";
import Button from "@mui/material/Button";
import {Typography} from "@mui/material";
import {makeStyles} from "@mui/styles";
import { MainContentState } from "../../interfaces/MainContentState";

const useStyles = makeStyles({
    categoryButtonStyle: {
        textTransform: 'none !important' as 'none',
    },
});

interface CategoryNavbarButtonProps {
    title: string,
    value: MainContentState,
    currState: MainContentState,
    onSelection: (selection: MainContentState) => void,
}

const CategoryNavbarButton = ({title, value, currState, onSelection}: CategoryNavbarButtonProps) => {
    const {categoryButtonStyle} = useStyles();
    return <Button onClick={() => onSelection(value)}
                   disabled={currState === value}
                   className={categoryButtonStyle}
                   color="secondary">
        <Typography fontWeight="bold">
            {title}
        </Typography>
    </Button>;
}

export default CategoryNavbarButton;