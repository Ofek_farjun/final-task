import React from "react";
import Clock from "../Clock";
import {AppBar, Avatar, Divider, Grid, IconButton, Toolbar, Typography} from "@mui/material";
import Brightness4Icon from '@mui/icons-material/Brightness4';
import Brightness7Icon from '@mui/icons-material/Brightness7';
import {makeStyles} from "@mui/styles";
import CategoryNavbarButton from "./CategoryNavbarButton";
import {ColorModeContext} from "../MainTheme";
import { MainContentState } from "../../interfaces/MainContentState";

interface NavbarProps {
    currState: MainContentState,
    onSelection: (selection: MainContentState) => void,
}

const useStyles = makeStyles({
    dividerStyle: {
        marginTop: '1em !important',
        marginBottom: '1em !important',
    },
    categoryButtonStyle: {
        textTransform: 'none !important' as 'none',
    },
    avatarStyle: {
        marginRight: '1em',
    }
});

const Navbar = ({currState, onSelection}: NavbarProps) => {
    const {dividerStyle, avatarStyle} = useStyles();
    const {colorMode, toggleColorMode} = React.useContext(ColorModeContext);

    return (
        <AppBar position="static">
            <Toolbar>
                <Grid container justifyContent="space-between" alignItems="center">
                    <Grid item>
                        <Grid container justifyContent="center" alignItems="center">
                            <Avatar src="/avatar.png" className={avatarStyle}/>
                            <Typography variant="h6" fontWeight="bold" marginY="1em">
                                Blue calender
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid item alignItems="center">
                        <CategoryNavbarButton title="Tasks and events for today" value={MainContentState.TasksAndEvents}
                                              currState={currState} onSelection={onSelection}/>
                    </Grid>
                    <Divider orientation="vertical" flexItem className={dividerStyle}/>
                    <Grid item alignItems="center">
                        <CategoryNavbarButton title="Tasks" value={MainContentState.Tasks}
                                              currState={currState} onSelection={onSelection}/>
                    </Grid>
                    <Divider orientation="vertical" flexItem className={dividerStyle}/>
                    <Grid item alignItems="center">
                        <CategoryNavbarButton title="Events" value={MainContentState.Events}
                                              currState={currState} onSelection={onSelection}/>
                    </Grid>
                    <Grid item display="flex" justifyContent="center" alignItems="center">
                        <Clock/>
                        <IconButton sx={{ml: 1}} onClick={toggleColorMode}>
                            {colorMode === 'dark' ? <Brightness7Icon/> : <Brightness4Icon/>}
                        </IconButton>
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    );
}

export default Navbar;