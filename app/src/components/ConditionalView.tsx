import React from "react";

interface ConditionalViewProps {
    show: boolean,
    children: React.ReactNode,
}

const ConditionalView = ({show, children}: ConditionalViewProps) => show ? <>{children}</> : null;

export default ConditionalView;