import React from "react";
import AssignmentOpen from '@mui/icons-material/Assignment';
import AssignmentInProgress from '@mui/icons-material/AssignmentLate';
import AssignmentClosed from '@mui/icons-material/AssignmentTurnedIn';
import EventIcon from '@mui/icons-material/NotificationsNone';
import Task, { isTask, Status } from "../interfaces/TaskInterface";
import Item from "../interfaces/ItemInterface";

interface TypeFieldProps {
    item: Item,
}

const TypeField = ({ item }: TypeFieldProps) => {
    if (isTask(item)) {
        switch ((item as Task).status) {
            case Status.OPEN:
                return <AssignmentOpen />;
            case Status.IN_PROGRESS:
                return <AssignmentInProgress htmlColor="orange" />;
            case Status.CLOSE:
                return <AssignmentClosed htmlColor="green" />;
            default:
                return <>Unknown</>;
        }
    } else {
        return <EventIcon />;
    }
}

export default TypeField;