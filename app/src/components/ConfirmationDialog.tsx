import React from "react";
import {Box, Button, Typography} from "@mui/material";

interface ConfirmationDialogProps {
    title: string,
    onDecision: (decision: boolean) => any,
}

const ConfirmationDialog = ({title, onDecision}: ConfirmationDialogProps) => (
    <>
        <Typography variant="h6" margin="1em">
            {title}
        </Typography>
        <Box display="flex" justifyContent="space-between">
            <Button onClick={() => onDecision(false)}>No</Button>
            <Button onClick={() => onDecision(true)}>Yes</Button>
        </Box>
    </>
);

export default ConfirmationDialog;