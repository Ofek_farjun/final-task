import React from "react";
import Snackbar from "@mui/material/Snackbar";
import {Box} from "@mui/material";

interface SnackbarAlertProps {
    isShowing: boolean,
    onClose: () => void,
    children: React.ReactNode,
}

const SnackbarAlert = ({isShowing, onClose, children}: SnackbarAlertProps) =>
    <Snackbar
        open={isShowing}
        anchorOrigin={{vertical: 'top', horizontal: 'center'}}
        autoHideDuration={6000}
        onClose={onClose}
    >
        <Box>
            {children}
        </Box>
    </Snackbar>;

export default SnackbarAlert;