import React, {useState} from "react";
import {createTheme, ThemeProvider} from "@mui/material/styles";

const ColorPalette = {
    primary: '#5497A7',
    secondary: '#E0DDDD',
}

interface MainThemeProps {
    children: React.ReactNode
}

export interface ColorModeContextProps {
    colorMode: 'light' | 'dark',
    toggleColorMode: () => void,
}

export const ColorModeContext = React.createContext<ColorModeContextProps>({
    colorMode: 'light',
    toggleColorMode: () => console.error('No EventsProvider provided'),
});

const MainTheme = ({children}: MainThemeProps) => {
    const [colorMode, setColorMode] = useState<'light' | 'dark'>('light');
    const toggleColorMode = () => setColorMode(colorMode === 'light' ? 'dark' : 'light');

    const theme = (colorMode: 'light' | 'dark') => createTheme({
        palette: {
            mode: colorMode,
            primary: {
                main: ColorPalette.primary,
            },
            secondary: {
                main: ColorPalette.secondary
            }
        },
    });

    return <ColorModeContext.Provider value={{colorMode, toggleColorMode}}>
        <ColorModeContext.Consumer>
            {
                ({colorMode}) => <ThemeProvider theme={theme(colorMode)}>
                    {children}
                </ThemeProvider>
            }
        </ColorModeContext.Consumer>
    </ColorModeContext.Provider>;
};

export default MainTheme;