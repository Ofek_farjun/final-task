import { ApolloClient, ApolloLink, ApolloProvider, from, HttpLink, InMemoryCache } from '@apollo/client';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { APOLLO_SERVER_URL, APOLLO_SUBSCRIPTION_SERVER_URL } from './graphql/consts';
import App from './App';
import store from './store/Store';
import { DialogProvider } from './components/Dialog/DialogContext';
import MainTheme from './components/MainTheme';
import { getMainDefinition } from '@apollo/client/utilities';
import { WebSocketLink } from '@apollo/client/link/ws';
import { RetryLink } from '@apollo/client/link/retry';

const httpLink = new HttpLink({
  uri: APOLLO_SERVER_URL,
});

const wsLink = new WebSocketLink({
  uri: APOLLO_SUBSCRIPTION_SERVER_URL,
  options: { reconnect: true, },
});

const link = new RetryLink({
  attempts: {
    max: Infinity,
  },
}).split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    );
  },
  wsLink,
  httpLink,
);

export const client = new ApolloClient({
  link,
  cache: new InMemoryCache(),
  queryDeduplication: true,
});

ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <Provider store={store}>
        <MainTheme>
          <DialogProvider>
            <App />
          </DialogProvider>
        </MainTheme>
      </Provider>
    </ApolloProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
