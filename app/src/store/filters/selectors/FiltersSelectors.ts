import { createSelector } from "@reduxjs/toolkit";
import { StoreState } from "../../Store";
import { FiltersState } from "../FiltersStore";

const filtersStoreSelector = (state: StoreState) => state.FiltersStore;

export const filtersSelector = createSelector(filtersStoreSelector, (state: FiltersState) => state);
