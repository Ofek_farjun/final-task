import { PayloadAction } from "@reduxjs/toolkit";
import { combineEpics, Epic, ofType, StateObservable } from "redux-observable";
import { debounceTime, mergeMap } from "rxjs";
import { StoreState } from "../../Store";
import { SET_SELECTED_QUICK_FILTERS, SET_TXT_FILTER } from "../actions/FiltersActions";
import { FiltersState, setFiltersStoreAction } from "../FiltersStore";

export const setSelectedQuickFiltersEpic: Epic = (action$, state$: StateObservable<StoreState>) => action$.pipe(
    ofType(SET_SELECTED_QUICK_FILTERS),
    mergeMap(({ payload }: PayloadAction<FiltersState>) => [
        setFiltersStoreAction({
            ...payload,
            searchFilter: state$.value.FiltersStore.searchFilter,
        }),
    ]),
);

export const setTxtFilterEpic: Epic = (action$, state$: StateObservable<StoreState>) => action$.pipe(
    ofType(SET_TXT_FILTER),
    debounceTime(100),
    mergeMap(({ payload }: PayloadAction<string>) => [
        setFiltersStoreAction({
            ...state$.value.FiltersStore,
            searchFilter: payload,
        }),
    ]),
);

export default combineEpics(
    setTxtFilterEpic,
    setSelectedQuickFiltersEpic,
);