import { createSlice } from "@reduxjs/toolkit";
import FiltersReducers from "./reducers/FiltersReducers";

export interface FiltersState {
    searchFilter: string,
    onlyTasks: boolean,
    onlyEvents: boolean,
    uncompletedTasks: boolean,
    highPriorityTasks: boolean,
    eventsForToday: boolean,
    futureEvents: boolean,
}

export const initialFiltersState: FiltersState = {
    searchFilter: '',
    onlyTasks: false,
    onlyEvents: false,
    uncompletedTasks: false,
    highPriorityTasks: false,
    eventsForToday: false,
    futureEvents: false
};

const filtersSlice = createSlice({
    name: 'Filters',
    initialState: initialFiltersState,
    reducers: FiltersReducers,
});

export default filtersSlice.reducer;

export const { setFiltersStoreAction } = filtersSlice.actions;