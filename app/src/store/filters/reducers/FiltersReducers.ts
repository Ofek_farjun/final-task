import { PayloadAction } from "@reduxjs/toolkit";
import { FiltersState } from "../FiltersStore";

const setFiltersStoreAction = (state: FiltersState, { payload }: PayloadAction<FiltersState>) => {
    Object.assign(state, payload);
}

export default {
    setFiltersStoreAction,
};