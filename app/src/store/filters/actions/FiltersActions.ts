import { Action, PayloadAction } from "@reduxjs/toolkit";
import { FiltersState } from "../FiltersStore";

export const SET_SELECTED_QUICK_FILTERS = 'setQuickFiltersAction';
export const SET_TXT_FILTER = 'setTxtFilterAction';
export const CLEAR_FILTERS = 'clearFiltersAction';

export const setSearchFilterAction = (searchFilter: string): PayloadAction<string> => ({
    type: SET_TXT_FILTER,
    payload: searchFilter,
});

export const setSelectedQuickFiltersAction = (filters: FiltersState): PayloadAction<FiltersState> => ({
    type: SET_SELECTED_QUICK_FILTERS,
    payload: filters,
});