import { applyMiddleware, combineReducers, createStore } from "redux";
import { combineEpics, createEpicMiddleware } from "redux-observable";
import FiltersStore from "./filters/FiltersStore";
import FiltersEpics from "./filters/epics/FiltersEpics";

export const rootEpic = combineEpics(
    FiltersEpics,
);

const epicMiddleware = createEpicMiddleware();

const reducer = combineReducers({
    FiltersStore,
});

const store = createStore(
    reducer,
    applyMiddleware(epicMiddleware)
);

epicMiddleware.run(rootEpic);

export type StoreState = ReturnType<typeof store.getState>;

export const StoreSelector = (state: StoreState) => state;

export default store;