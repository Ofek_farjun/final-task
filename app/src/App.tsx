import { useEffect, useState } from 'react';
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import { makeStyles } from "@mui/styles";
import Navbar from "./components/Navbar/Navbar";
import { useDispatch } from 'react-redux';
import GenericItemsTable from './components/GenericTable/GenericItemsTable';
import { initialFiltersState } from './store/filters/FiltersStore';
import { MainContentState } from './interfaces/MainContentState';
import { setSelectedQuickFiltersAction } from './store/filters/actions/FiltersActions';

const useStyles = makeStyles({
    appStyle: {
        height: '100%'
    },
    contentStyle: {
        borderRadius: '1em',
        height: '100%',
    },
});

const App = () => {
    const dispatch = useDispatch();
    const { appStyle, contentStyle } = useStyles();
    const [mainState, setMainState] = useState<MainContentState>(MainContentState.TasksAndEvents);

    useEffect(() => {
        const filtersState = { ...initialFiltersState };
        switch (mainState) {
            case MainContentState.TasksAndEvents:
                filtersState.eventsForToday = true;
                break;
            case MainContentState.Tasks:
                filtersState.onlyTasks = true;
                break;
            case MainContentState.Events:
                filtersState.onlyEvents = true;
        }
        dispatch(setSelectedQuickFiltersAction(filtersState));
    }, [mainState]);

    return <>
        <Box className={appStyle}>
            <CssBaseline />
            <Box display="flex" flexDirection="column" className={contentStyle}>
                <Navbar onSelection={selection => setMainState(selection)}
                    currState={mainState} />
                <GenericItemsTable
                    tableType={mainState} />
            </Box>
        </Box>
    </>
};

export default App;
