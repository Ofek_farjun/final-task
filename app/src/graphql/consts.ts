export const API_ROOT_URL = 'http://localhost:3001';
export const APOLLO_SERVER_URL = 'http://localhost:4000/graphql';
export const APOLLO_SUBSCRIPTION_SERVER_URL = 'ws://localhost:4000/graphql';