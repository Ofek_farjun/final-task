import { gql } from "@apollo/client";

export const GET_TASKS = gql`
  query GetTasks($taskFilters: TaskFilters!) {
    tasks(filters: $taskFilters) {
      id
      title
      description
      estimatedTime
      status
      priority
      review
      timeSpent
      untilDate
    }
  }
`;