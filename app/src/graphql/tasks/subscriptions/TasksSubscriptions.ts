import { gql } from "@apollo/client";
import Task from "../../../interfaces/TaskInterface";

export interface TaskSubscriptionResult {
  taskSubscription: {
    mutation: string,
    task?: Task,
    deletedId?: string,
  }
}

export const TASK_SUBSCRIPTION = gql`
  subscription($filters: TaskFilters!) {
    taskSubscription(filters: $filters) {
      mutation
      task {
        id
        title
        description
        estimatedTime
        status
        priority
        review
        timeSpent
        untilDate
      }
      deletedId
    }
  }
`;
