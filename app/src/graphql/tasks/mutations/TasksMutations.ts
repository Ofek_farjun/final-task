import { gql } from "@apollo/client";

export const ADD_TASK = gql`
  mutation AddTask($data: TaskData!) {
    addTask(data: $data) {
      id
      title
      description
      estimatedTime
      status
      priority
      review
      timeSpent
      untilDate
    }
  }
`;

export const UPDATE_TASK = gql`
  mutation UpdateTask($data: TaskData!) {
    updateTask(data: $data) {
      id
      title
      description
      estimatedTime
      status
      priority
      review
      timeSpent
      untilDate
    }
  }
`;

export const DELETE_TASK = gql`
  mutation DeleteTask($id: String!) {
    deleteTask(id: $id)
  }
`;
