import { gql } from "@apollo/client";

export const GET_EVENTS = gql`
  query GetEvents($eventFilters: EventFilters!) {
    events(filters: $eventFilters) {
        id,
        title,
        description,
        beginningTime,
        endingTime,
        color,
        location,
        invitedGuests,
        notificationTime
    }
  }
`;