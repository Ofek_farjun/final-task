import gql from "graphql-tag";
import Event from "../../../interfaces/EventInterface";

export interface EventSubscriptionResult {
  eventSubscription: {
    mutation: string,
    event?: Event,
    deletedId?: string,
  }
}

export const EVENT_SUBSCRIPTION = gql`
  subscription onEventCreated($filters: EventFilters!) {
    eventSubscription(filters: $filters) {
      mutation,
      event {
        id,
        title,
        description,
        beginningTime,
        endingTime,
        color,
        location,
        invitedGuests,
        notificationTime
      },
      deletedId
    }
  }
`;
