import { gql } from "@apollo/client";

export const ADD_EVENT = gql`
  mutation AddEvent($data: EventData!) {
    addEvent(data: $data) {
      id,
      title,
      description,
      beginningTime,
      endingTime,
      color,
      location,
      invitedGuests,
      notificationTime
    }
  }
`;

export const UPDATE_EVENT = gql`
  mutation UpdateEvent($data: EventData!) {
    updateEvent(data: $data) {
      id,
      title,
      description,
      beginningTime,
      endingTime,
      color,
      location,
      invitedGuests,
      notificationTime
    }
  }
`;

export const DELETE_EVENT = gql`
  mutation DeleteEvent($id: String!) {
    deleteEvent(id: $id)
  }
`;
