import { gql } from "@apollo/client";

export const GET_ITEMS = gql`
  query GetItems($eventFilters: EventFilters!, $taskFilters: TaskFilters!) {
    events(filters: $eventFilters) {
        id,
        title,
        description,
        beginningTime,
        endingTime,
        color,
        location,
        invitedGuests,
        notificationTime
    }
    tasks(filters: $taskFilters) {
      id
      title
      description
      estimatedTime
      status
      priority
      review
      timeSpent
      untilDate
    }
  }
`;