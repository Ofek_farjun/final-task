import { useMutation, useQuery, useSubscription } from "@apollo/client";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { MutationType } from "../components/Utils/SubscriptionUtils";
import { ADD_EVENT, UPDATE_EVENT, DELETE_EVENT } from "../graphql/events/mutations/EventsMutations";
import { GET_EVENTS } from "../graphql/events/queries/EventsQueries";
import { EventSubscriptionResult, EVENT_SUBSCRIPTION } from "../graphql/events/subscriptions/EventsSubscriptions";
import { GET_ITEMS } from "../graphql/items/ItemQueries";
import { ADD_TASK, UPDATE_TASK, DELETE_TASK } from "../graphql/tasks/mutations/TasksMutations";
import { GET_TASKS } from "../graphql/tasks/queries/TasksQueries";
import { TaskSubscriptionResult, TASK_SUBSCRIPTION } from "../graphql/tasks/subscriptions/TasksSubscriptions";
import Item from "../interfaces/ItemInterface";
import Task, { isTask } from "../interfaces/TaskInterface";
import Event, { isEvent } from "../interfaces/EventInterface";
import { filtersSelector } from "../store/filters/selectors/FiltersSelectors";

export const useItems = () => {
    const [items, setItems] = useState<Item[]>([]);
    const filters = useSelector(filtersSelector);

    const [addTask] = useMutation(ADD_TASK);
    const [addEvent] = useMutation(ADD_EVENT);
    const [updateTask] = useMutation(UPDATE_TASK);
    const [updateEvent] = useMutation(UPDATE_EVENT);
    const [deleteTask] = useMutation(DELETE_TASK);
    const [deleteEvent] = useMutation(DELETE_EVENT);

    useQuery<{ tasks?: Task[], events?: Event[] }>(
        filters.onlyEvents ? GET_EVENTS : (filters.onlyTasks ? GET_TASKS : GET_ITEMS),
        {
            fetchPolicy: 'no-cache',
            variables: {
                eventFilters: {
                    searchFilter: filters.searchFilter,
                    eventsForToday: filters.eventsForToday,
                    futureEvents: filters.futureEvents,
                },
                taskFilters: {
                    searchFilter: filters.searchFilter,
                    uncompletedTasks: filters.uncompletedTasks,
                    highPriorityTasks: filters.highPriorityTasks,
                },
            },
            onCompleted: ({ tasks, events }) => setItems([...(tasks || []), ...(events || [])])
        },
    );

    useSubscription<TaskSubscriptionResult>(TASK_SUBSCRIPTION, {
        variables: {
            filters: {
                searchFilter: filters.searchFilter,
                uncompletedTasks: filters.uncompletedTasks,
                highPriorityTasks: filters.highPriorityTasks,
            },
        },
        skip: filters.onlyEvents,
        onSubscriptionData: ({ subscriptionData: { data } }) => {
            const dataReceived = data?.taskSubscription;
            switch (dataReceived?.mutation) {
                case MutationType.Add:
                    dataReceived.task && addItemToState(dataReceived.task);
                    break;
                case MutationType.Update:
                    dataReceived.task && updateItemInState(dataReceived.task);
                    break;
                case MutationType.Delete:
                    dataReceived.deletedId && removeItemInState(dataReceived.deletedId);
                    break;
            }
        },
    });

    useSubscription<EventSubscriptionResult>(EVENT_SUBSCRIPTION, {
        variables: {
            filters: {
                searchFilter: filters.searchFilter,
                eventsForToday: filters.eventsForToday,
                futureEvents: filters.futureEvents,
            },
        },
        skip: filters.onlyTasks,
        onSubscriptionData: ({ subscriptionData: { data } }) => {
            const dataReceived = data?.eventSubscription;
            switch (dataReceived?.mutation) {
                case MutationType.Add:
                    dataReceived.event && addItemToState(dataReceived.event);
                    break;
                case MutationType.Update:
                    dataReceived.event && updateItemInState(dataReceived.event);
                    break;
                case MutationType.Delete:
                    dataReceived.deletedId && removeItemInState(dataReceived.deletedId);
                    break;
            }
        }
    });

    const addItemToState = (item: Item) => setItems([...items, item]);
    const updateItemInState = (updatedItem: Item) => setItems(items.map(
        item => item.id === updatedItem.id ? updatedItem : item
    ));
    const removeItemInState = (id: string) => setItems(items.filter(
        item => item.id !== id
    ));

    const createItem = (item: Item) => {
        if (isTask(item)) {
            addTask({ variables: { data: item } });
        } else if (isEvent(item)) {
            addEvent({ variables: { data: item } });
        }
    }

    const updateItem = (item: Item) => {
        if (isTask(item)) {
            updateTask({ variables: { data: item } });
        } else if (isEvent(item)) {
            updateEvent({ variables: { data: item } });
        }
    }

    const deleteItem = (item: Item) => {
        if (isTask(item)) {
            deleteTask({ variables: { id: item.id } });
        } else if (isEvent(item)) {
            deleteEvent({ variables: { id: item.id } });
        }
    }

    return {
        items,
        createItem,
        updateItem,
        deleteItem
    }
}