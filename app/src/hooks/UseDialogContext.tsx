import React, {useContext} from "react";
import DialogContext from "../components/Dialog/DialogContext";

export default function useDialogContext() {
    return useContext(DialogContext);
}