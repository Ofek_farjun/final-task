import React, {useEffect} from "react";

export default function useAsync<T>(asyncFnc: () => Promise<T>, onSuccess: (data: T) => void,
                                    onError: (data: any) => void) {
    let isActive = true;
    useEffect(() => {
        asyncFnc().then((data) => isActive && onSuccess(data))
            .catch(exception => isActive && onError(exception));

        return () => {
            isActive = false
        };
    }, []);
};