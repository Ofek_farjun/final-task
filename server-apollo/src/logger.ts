import SimpleNodeLogger from 'simple-node-logger';

export default SimpleNodeLogger.createSimpleLogger({
    logFilePath: process.env.MANAGEMENT_LOG_FILE || 'log.log',
    timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS',
});
