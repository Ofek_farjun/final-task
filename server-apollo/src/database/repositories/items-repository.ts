import { ObjectId } from 'mongodb';
import { DatabaseError } from '../../errors/DatabaseError';
import { Event, Item, Task } from '../../generated/graphql';
import { getCollection } from '../../utils/db-utils';
import { isEvent } from '../../utils/event-utils';
import { isTask } from '../../utils/task-utils';
import {
    validateEventObj,
    validateTaskObj,
} from '../../utils/validation-utils';

const EVENTS_COLLECTION = 'events';
const TASKS_COLLECTION = 'tasks';

type ItemDocument<T extends Item> = Omit<T, 'id'> & { _id: ObjectId };

const toItemDocument = <T extends Item>({
    id,
    ...itemData
}: T): ItemDocument<T> => ({
    ...itemData,
    _id: new ObjectId(id),
});

const toItem = <T extends Item>({ _id, ...itemData }: ItemDocument<T>) => ({
    ...itemData,
    id: _id.toString(),
});

const getItemCollection = <T extends Item>(item: T) =>
    getCollection(isTask(item) ? TASKS_COLLECTION : EVENTS_COLLECTION);

const validateItem = async (item: Item) =>
    (isTask(item) && (await validateTaskObj(item))) ||
    (isEvent(item) && (await validateEventObj(item)));

export const getEvents = async (): Promise<Event[]> => {
    return (
        await getCollection(EVENTS_COLLECTION)
            .find<ItemDocument<Event>>({})
            .toArray()
    ).map((item) => toItem<Event>(item));
};

export const getTasks = async (): Promise<Task[]> => {
    return (
        await getCollection(TASKS_COLLECTION)
            .find<ItemDocument<Task>>({})
            .toArray()
    ).map((item) => toItem<Task>(item));
};

export const addItem = async <T extends Item>(item: T): Promise<T> => {
    await validateItem(item);
    const { id, ...itemData } = item;
    const { acknowledged, insertedId } = await getItemCollection(
        item
    ).insertOne(itemData);
    if (!acknowledged) throw new DatabaseError(`Could not insert event`);
    return {
        ...item,
        id: insertedId.toString(),
    };
};

export const updateItem = async <T extends Item>(item: T): Promise<T> => {
    await validateItem(item);
    const eventDocument = toItemDocument(item);
    const { acknowledged } = await getItemCollection(item).updateOne(
        { _id: eventDocument._id },
        {
            $set: eventDocument,
        }
    );
    if (!acknowledged) throw new DatabaseError(`Could not update event`);
    return { ...item };
};

const deleteItem = async (id: string, collectionName: string) => {
    const { acknowledged } = await getCollection(collectionName).deleteOne({
        _id: id,
    });
    if (!acknowledged) throw new DatabaseError(`Could not delete event`);
    return id;
};

export const deleteEvent = (id: string) => deleteItem(id, EVENTS_COLLECTION);

export const deleteTask = (id: string) => deleteItem(id, TASKS_COLLECTION);
