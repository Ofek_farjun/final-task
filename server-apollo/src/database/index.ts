import 'dotenv/config';
import logger from '../logger';
import { Db, MongoClient, ServerApiVersion } from 'mongodb';

const client = new MongoClient(process.env.MONGO_URL || '', {
    serverApi: ServerApiVersion.v1,
    auth: {
        username: process.env.MONGO_USER,
        password: process.env.MONGO_PASS,
    },
});

let dbConnection: Db;

export const connectToDB = async () => {
    await client.connect();
    dbConnection = client.db(process.env.MONGO_DB_NAME || 'none');
    logger.info('DB initialized successfully');
};

export const getDB = () => dbConnection;
