import { eventQueries } from './resolvers/events';
import { taskQueries } from './resolvers/tasks';

export default {
    ...taskQueries,
    ...eventQueries,
};
