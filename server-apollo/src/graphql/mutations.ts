import { eventMutations } from './resolvers/events';
import { taskMutations } from './resolvers/tasks';

export default {
    ...taskMutations,
    ...eventMutations,
};
