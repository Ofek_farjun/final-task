import { GraphQLEnumType } from 'graphql';

const TaskStatusEnum = new GraphQLEnumType({
    name: 'TaskStatus',
    values: {
        OPEN: {
            value: 'Open',
        },
        IN_PROGRESS: {
            value: 'In Progress',
        },
        CLOSE: {
            value: 'Close',
        },
    },
});

export default TaskStatusEnum;
