import { GraphQLEnumType } from 'graphql';

const TaskPriorityEnum = new GraphQLEnumType({
    name: 'TaskPriority',
    values: {
        HIGH: {
            value: 'High',
        },
        MEDIUM: {
            value: 'Medium',
        },
        LOW: {
            value: 'Low',
        },
    },
});

export default TaskPriorityEnum;
