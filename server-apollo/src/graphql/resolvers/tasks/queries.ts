import { getTasks } from '../../../database/repositories/items-repository';
import { QueryResolvers, Task } from '../../../generated/graphql';
import logger from '../../../logger';
import { queryInfo } from '../../../utils/logging-utils';
import { filterItems } from '../../../utils/quick-filters-utils';
import { ContextType } from '../../types/context';

const taskQueries: QueryResolvers<ContextType> = {
    tasks: async (_, { filters }) => {
        logger.info(queryInfo(`Fetch tasks`, { filters }));
        const tasks = await getTasks();
        return filterItems<Task>(filters, tasks);
    },
};

export default taskQueries;
