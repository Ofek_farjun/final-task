import { IMiddleware } from 'graphql-middleware';
import { MutationAddTaskArgs, Task } from '../../../generated/graphql';
import { TaskFactory } from '../../../utils/task-utils';

export interface PostTaskArgs {
    task: Task;
}

const postTaskMiddleware: IMiddleware = async (
    resolve,
    parent,
    args: MutationAddTaskArgs,
    context,
    info
) => {
    const postArgs: PostTaskArgs = {
        task: TaskFactory.createTask(args.data),
    };
    return await resolve(parent, postArgs, context, info);
};

const taskMiddlewares = {
    Mutation: {
        addTask: postTaskMiddleware,
        updateTask: postTaskMiddleware,
    },
};

export default taskMiddlewares;
