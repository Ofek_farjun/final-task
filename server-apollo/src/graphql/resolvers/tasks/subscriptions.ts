import {
    SubscriptionResolvers,
    SubscriptionTaskSubscriptionArgs,
    TaskSubscriptionType,
} from '../../../generated/graphql';
import { withFilter } from 'graphql-subscriptions';
import { isItemIncludedInFilters } from '../../../utils/quick-filters-utils';
import { ContextType } from '../../types/context';

export const TASK_MUTATION = 'TASK_MUTATION';

const taskSubscriptions: SubscriptionResolvers<ContextType> = {
    taskSubscription: {
        subscribe: withFilter(
            (_, __, { pubsub }: ContextType) =>
                pubsub.asyncIterator([TASK_MUTATION]),
            (
                {
                    taskSubscription,
                }: { taskSubscription: TaskSubscriptionType },
                { filters }: SubscriptionTaskSubscriptionArgs
            ) =>
                !taskSubscription.task ||
                isItemIncludedInFilters(taskSubscription.task, filters)
        ),
    },
};

export default taskSubscriptions;
