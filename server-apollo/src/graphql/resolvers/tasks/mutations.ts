import {
    addItem,
    deleteTask,
    updateItem,
} from '../../../database/repositories/items-repository';
import {
    MutationDeleteTaskArgs,
    MutationType,
    TaskSubscriptionType,
} from '../../../generated/graphql';
import logger from '../../../logger';
import { mutationInfo } from '../../../utils/logging-utils';
import { ContextType } from '../../types/context';
import { PostTaskArgs } from './middlewares';
import { TASK_MUTATION } from './subscriptions';

const taskMutations = {
    addTask: async (
        _: never,
        { task }: PostTaskArgs,
        { pubsub }: ContextType
    ) => {
        logger.info(mutationInfo(`addTask`, task));
        const res = await addItem(task);
        const taskSubscription: TaskSubscriptionType = {
            mutation: MutationType.Add,
            task: res,
        };
        pubsub.publish(TASK_MUTATION, { taskSubscription });
        return res;
    },
    updateTask: async (
        _: never,
        { task }: PostTaskArgs,
        { pubsub }: ContextType
    ) => {
        logger.info(mutationInfo(`updateTask`, task));
        const res = await updateItem(task);
        const taskSubscription: TaskSubscriptionType = {
            mutation: MutationType.Update,
            task: res,
        };
        pubsub.publish(TASK_MUTATION, { taskSubscription });
        return res;
    },
    deleteTask: async (
        _: never,
        { id }: MutationDeleteTaskArgs,
        { pubsub }: ContextType
    ) => {
        logger.info(mutationInfo(`deleteTask`, { id }));
        const res = await deleteTask(id);
        const taskSubscription: TaskSubscriptionType = {
            mutation: MutationType.Delete,
            deletedId: res,
        };
        pubsub.publish(TASK_MUTATION, { taskSubscription });
        return res;
    },
};

export default taskMutations;
