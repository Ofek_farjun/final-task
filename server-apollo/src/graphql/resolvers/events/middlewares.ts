import { IMiddleware } from 'graphql-middleware';
import { Event, MutationAddEventArgs } from '../../../generated/graphql';
import { EventFactory } from '../../../utils/event-utils';

export interface PostEventArgs {
    event: Event;
}

const postEventMiddleware: IMiddleware = async (
    resolve,
    parent,
    args: MutationAddEventArgs,
    context,
    info
) => {
    const postArgs: PostEventArgs = {
        event: EventFactory.createEvent(args.data),
    };
    return await resolve(parent, postArgs, context, info);
};

const eventMiddlewares = {
    Mutation: {
        addEvent: postEventMiddleware,
        updateEvent: postEventMiddleware,
    },
};

export default eventMiddlewares;
