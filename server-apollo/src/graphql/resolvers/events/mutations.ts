import {
    addItem,
    deleteEvent,
    updateItem,
} from '../../../database/repositories/items-repository';
import {
    EventSubscriptionType,
    MutationDeleteEventArgs,
    MutationType,
} from '../../../generated/graphql';
import logger from '../../../logger';
import { mutationInfo } from '../../../utils/logging-utils';
import { ContextType } from '../../types/context';
import { PostEventArgs } from './middlewares';
import { EVENT_MUTATION } from './subscriptions';

const eventMutations = {
    addEvent: async (
        _: never,
        { event }: PostEventArgs,
        { pubsub }: ContextType
    ) => {
        logger.info(mutationInfo(`addEvent`, event));
        const res = await addItem(event);
        const eventSubscription: EventSubscriptionType = {
            mutation: MutationType.Add,
            event: res,
        };
        pubsub.publish(EVENT_MUTATION, { eventSubscription });
        return res;
    },
    updateEvent: async (
        _: never,
        { event }: PostEventArgs,
        { pubsub }: ContextType
    ) => {
        logger.info(mutationInfo(`updateEvent`, event));
        const res = await updateItem(event);
        const eventSubscription: EventSubscriptionType = {
            mutation: MutationType.Update,
            event: res,
        };
        pubsub.publish(EVENT_MUTATION, { eventSubscription });
        return res;
    },
    deleteEvent: async (
        _: never,
        { id }: MutationDeleteEventArgs,
        { pubsub }: ContextType
    ) => {
        logger.info(mutationInfo(`deleteEvent`, { id }));
        const res = await deleteEvent(id);
        const eventSubscription: EventSubscriptionType = {
            mutation: MutationType.Delete,
            deletedId: res,
        };
        pubsub.publish(EVENT_MUTATION, { eventSubscription });
        return res;
    },
};

export default eventMutations;
