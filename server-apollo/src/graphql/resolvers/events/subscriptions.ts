import {
    EventSubscriptionType,
    SubscriptionEventSubscriptionArgs,
    SubscriptionResolvers,
} from '../../../generated/graphql';
import { ContextType } from '../../types/context';
import { withFilter } from 'graphql-subscriptions';
import { isItemIncludedInFilters } from '../../../utils/quick-filters-utils';

export const EVENT_MUTATION = 'EVENT_MUTATION_EVENT';

const eventSubscriptions: SubscriptionResolvers<ContextType> = {
    eventSubscription: {
        subscribe: withFilter(
            (_, __, { pubsub }: ContextType) =>
                pubsub.asyncIterator([EVENT_MUTATION]),
            (
                {
                    eventSubscription,
                }: { eventSubscription: EventSubscriptionType },
                { filters }: SubscriptionEventSubscriptionArgs
            ) =>
                !eventSubscription.event ||
                isItemIncludedInFilters(eventSubscription.event, filters)
        ),
    },
};

export default eventSubscriptions;
