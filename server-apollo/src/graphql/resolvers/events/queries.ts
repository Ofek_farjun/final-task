import { getEvents } from '../../../database/repositories/items-repository';
import { Event, QueryResolvers } from '../../../generated/graphql';
import logger from '../../../logger';
import { queryInfo } from '../../../utils/logging-utils';
import { filterItems } from '../../../utils/quick-filters-utils';
import { ContextType } from '../../types/context';

const eventQueries: QueryResolvers<ContextType> = {
    events: async (_, { filters }) => {
        logger.info(queryInfo(`Fetch events`, { filters }));
        const events = await getEvents();
        return filterItems<Event>(filters, events);
    },
};

export default eventQueries;
