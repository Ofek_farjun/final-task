export default `

scalar TaskPriority
scalar TaskStatus

input TaskFilters {
    searchFilter: String!,
    uncompletedTasks: Boolean,
    highPriorityTasks: Boolean,
}

type Task implements Item {
    id: String!,
    title: String!,
    description: String!,
    estimatedTime: String!,
    status: TaskStatus!,
    priority: TaskPriority!,
    review: String,
    timeSpent: String,
    untilDate: ISODate
}

type Query {
    tasks(filters: TaskFilters!): [Task!],
}

input TaskData {
    id: String!,
    title: String!,
    description: String!,
    estimatedTime: String!,
    status: String!,
    priority: String!,
    review: String,
    timeSpent: String,
    untilDate: String
}

type Mutation {
    addTask(data: TaskData!): Task!,
    updateTask(data: TaskData!): Task!,
    deleteTask(id: String!): String!,
}

type TaskSubscriptionType {
    mutation: MutationType!,
    task: Task,
    deletedId: String,
}

type Subscription {
    taskSubscription(filters: TaskFilters!): TaskSubscriptionType!,
}
`;
