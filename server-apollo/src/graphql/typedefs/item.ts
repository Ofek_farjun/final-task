export default `
interface Item {
    id: String!,
    title: String!,
    description: String!,
}
`;
