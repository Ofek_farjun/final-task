export default `
input EventFilters {
    searchFilter: String!,
    futureEvents: Boolean,
    eventsForToday: Boolean
}

type Event implements Item {
    id: String!,
    title: String!,
    description: String!,
    beginningTime: ISODate!,
    endingTime: ISODate!,
    color: String!,
    location: String,
    invitedGuests: [String!]!,
    notificationTime: ISODate!
}

type Query {
    events(filters: EventFilters!): [Event!],
}

input EventData {
    id: String!,
    title: String!,
    description: String!,
    beginningTime: String!,
    endingTime: String!,
    color: String!,
    location: String,
    invitedGuests: [String!]!,
    notificationTime: String!
}

type Mutation {
    addEvent(data: EventData!): Event!,
    updateEvent(data: EventData!): Event!,
    deleteEvent(id: String!): String!,
}

type EventSubscriptionType {
    mutation: MutationType!,
    event: Event,
    deletedId: String,
}

type Subscription {
    eventSubscription(filters: EventFilters!): EventSubscriptionType!,
}
`;
