import EventGraphql from './event';
import HeadersGraphql from './headers';
import TaskGraphql from './task';
import ItemGraphql from './item';

export default [HeadersGraphql, ItemGraphql, TaskGraphql, EventGraphql];
