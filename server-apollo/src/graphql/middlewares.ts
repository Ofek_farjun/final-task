import _ from 'lodash';
import eventMiddlewares from './resolvers/events/middlewares';
import taskMiddlewares from './resolvers/tasks/middlewares';

export default _.merge(taskMiddlewares, eventMiddlewares);
