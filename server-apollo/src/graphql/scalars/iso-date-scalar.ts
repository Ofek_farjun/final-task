import { GraphQLScalarType, Kind, ValueNode } from 'graphql';

const serialize = (value: unknown) =>
    value instanceof Date ? value.toISOString() : null;

const parseValue = (value: unknown) =>
    value instanceof String ? new Date(value.toString()) : null;

const parseLiteral = (ast: ValueNode) =>
    ast.kind === Kind.STRING ? parseValue(ast.value) : null;

export default new GraphQLScalarType({
    name: 'ISODate',
    description:
        'Convert ISO date formatted string to Date object and otherwise',
    serialize,
    parseValue,
    parseLiteral,
});
