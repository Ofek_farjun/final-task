import customResolvers from './custom-resolvers';
import mutations from './mutations';
import queries from './queries';
import subscriptions from './subscriptions';
import typedefs from './typedefs';

const properties = {
    typeDefs: typedefs,
    resolvers: {
        ...customResolvers,
        Query: queries,
        Mutation: mutations,
        Subscription: subscriptions,
    },
};

export default properties;
