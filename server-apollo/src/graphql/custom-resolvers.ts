import TaskPriorityEnum from './resolvers/tasks/enums/task-priority-enum';
import TaskStatusEnum from './resolvers/tasks/enums/task-status-enum';
import ISODateScalar from './scalars/iso-date-scalar';

export default {
    ISODate: ISODateScalar,
    TaskStatus: TaskStatusEnum,
    TaskPriority: TaskPriorityEnum,
};
