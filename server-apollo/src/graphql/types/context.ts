import { PubSub } from 'graphql-subscriptions';

export interface ContextType {
    pubsub: PubSub;
}
