import { eventSubscriptions } from './resolvers/events';
import { taskSubscriptions } from './resolvers/tasks';

export default {
    ...taskSubscriptions,
    ...eventSubscriptions,
};
