import 'dotenv/config';
import { ApolloServerPluginDrainHttpServer } from 'apollo-server-core';
import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import http from 'http';
import cors from 'cors';
import { connectToDB } from './database';
import graphQLProperties from './graphql';
import { execute, subscribe } from 'graphql';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { PubSub } from 'graphql-subscriptions';
import { applyMiddleware } from 'graphql-middleware';
import middlewares from './graphql/middlewares';
import logger from './logger';
import { ContextType } from './graphql/types/context';

logger.info(`
        --------------------------------
        -          STARTUP             -
        --------------------------------
`);

const PORT = process.env.PORT || 4000;

const app = express();
app.use(cors());

const wsServer = http.createServer(app);

try {
    await connectToDB();

    const pubsub = new PubSub();

    const context = (): ContextType => ({
        pubsub,
    });

    const schema = makeExecutableSchema({
        typeDefs: graphQLProperties.typeDefs,
        resolvers: graphQLProperties.resolvers,
    });

    const schemaWithMiddlewares = applyMiddleware(schema, middlewares);

    const subscriptionServer = SubscriptionServer.create(
        {
            schema: schemaWithMiddlewares,
            subscribe,
            execute,
            onConnect: () => context(),
        },
        {
            server: wsServer,
            path: '/graphql',
        }
    );

    const server = new ApolloServer({
        schema: schemaWithMiddlewares,
        context,
        formatError: (error) => {
            logger.error(`GraphQL Action: ${error}`);
            return error;
        },
        plugins: [
            ApolloServerPluginDrainHttpServer({ httpServer: wsServer }),
            {
                async serverWillStart() {
                    return {
                        async drainServer() {
                            subscriptionServer.close();
                        },
                    };
                },
            },
        ],
    });

    await server.start();
    server.applyMiddleware({ app });

    await wsServer.listen(PORT);

    logger.info(`Server initiated successfully, Port: ${PORT}`);
} catch (error) {
    logger.error(`Could not initiate server: ${error}`);
}
