export const queryInfo = (msg: string, args: unknown) =>
    `GraphQL query: ${msg}, data: ${printData(args)}`;
export const mutationInfo = (msg: string, args: unknown) =>
    `GraphQL mutation: ${msg}, data: ${printData(args)}`;

export const printData = (data: unknown) => `---${JSON.stringify(data)}---`;
