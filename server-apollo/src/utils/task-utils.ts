import { Item, Task, TaskData } from '../generated/graphql';

export class TaskFactory {
    static createTask(dto: TaskData): Task {
        return {
            ...dto,
            untilDate: dto.untilDate ? new Date(dto.untilDate) : undefined,
        };
    }
}

export enum Status {
    OPEN = 'Open',
    IN_PROGRESS = 'In Progress',
    CLOSE = 'Close',
}

export enum Priority {
    LOW = 'Low',
    MEDIUM = 'Medium',
    HIGH = 'High',
}

export const isTask = (item: Item): item is Task => 'priority' in item;
