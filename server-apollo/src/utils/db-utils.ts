import { getDB } from '../database';

export const getCollection = (colletionName: string) => {
    const db = getDB();
    return db.collection(colletionName);
};
