import { EventData, Event, Item } from '../generated/graphql';

export class EventFactory {
    static createEvent(dto: EventData): Event {
        return {
            ...dto,
            beginningTime: new Date(dto.beginningTime),
            endingTime: new Date(dto.endingTime),
            location: dto.location || undefined,
            notificationTime: new Date(dto.notificationTime),
        };
    }
}

export const isEvent = (item: Item): item is Event => 'beginningTime' in item;
