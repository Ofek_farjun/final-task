import {
    Event,
    EventFilters,
    Item,
    Task,
    TaskFilters,
} from '../generated/graphql';
import { isTask, Status, Priority } from './task-utils';
import { isEvent } from './event-utils';

interface FiltersInput extends TaskFilters, EventFilters {}

const itemsIncludedInFilter = <T extends Item>(items: T[], filter: string) =>
    items.filter(
        (item) =>
            item.title.toLowerCase().includes(filter.toLowerCase()) ||
            item.description.toLowerCase().includes(filter.toLowerCase())
    );

const onlyTasks = (items: Item[]): Task[] => items.filter<Task>(isTask);
const onlyEvents = (items: Item[]): Event[] => items.filter<Event>(isEvent);

const uncompletedTasks = (items: Item[]) =>
    onlyTasks(items).filter((task) => task.status !== Status.CLOSE);

const highPriorityTasks = (items: Item[]) =>
    onlyTasks(items).filter((task) => task.priority === Priority.HIGH);

const eventsForToday = (items: Item[]) =>
    onlyEvents(items).filter(
        (event) =>
            event.beginningTime.toDateString() === new Date().toDateString()
    );

const futureEvents = (items: Item[]) =>
    onlyEvents(items).filter(
        (event) => event.beginningTime.getTime() > new Date().getTime()
    );

const quickFilterFunctions = (filters: FiltersInput) => {
    const filterFunctions: ((items: Item[]) => Item[])[] = [];

    filters.eventsForToday && filterFunctions.push(eventsForToday);
    filters.futureEvents && filterFunctions.push(futureEvents);
    filters.uncompletedTasks && filterFunctions.push(uncompletedTasks);
    filters.highPriorityTasks && filterFunctions.push(highPriorityTasks);

    return filterFunctions;
};

export const filterItems = <T extends Item>(
    filtersInput: FiltersInput,
    items: Item[]
): T[] => {
    let filteredItems: Item[] = itemsIncludedInFilter(
        [...items],
        filtersInput.searchFilter
    );
    quickFilterFunctions(filtersInput).map(
        (filterFunc) => (filteredItems = filterFunc(filteredItems))
    );

    return filteredItems as T[];
};

export const isItemIncludedInFilters = (
    item: Item,
    filtersInput: FiltersInput
) => filterItems(filtersInput, [item]).length !== 0;
