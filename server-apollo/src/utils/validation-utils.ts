import * as yup from 'yup';
import { Priority, Status } from './task-utils';
import { Event, Item, Task } from '../generated/graphql';

export const doesIdExists = (items: Item[], id: string) =>
    items.findIndex((item) => item.id === id) !== -1;

export const validateTaskObj = async (task: Task): Promise<Task> => {
    await yup
        .object()
        .shape({
            id: yup.string().required(),
            title: yup.string().required(),
            description: yup.string().required(),
            estimatedTime: yup.string().matches(ESTIMATED_TIME_REGEX),
            status: yup.mixed<Status>().oneOf(Object.values(Status)).required(),
            priority: yup
                .mixed<Priority>()
                .oneOf(Object.values(Priority))
                .required(),
            review: yup.string().notRequired(),
            timeSpent: yup.string().notRequired(),
            untilDate: yup.date().notRequired(),
        })
        .validate(task);

    return task;
};

export const validateEventObj = async (event: Event): Promise<Event> => {
    await yup
        .object()
        .shape({
            id: yup.string().required(),
            title: yup.string().required(),
            description: yup.string().required(),
            beginningTime: yup.date().required(),
            endingTime: yup.date().required(),
            color: yup.string().required(),
            location: yup.string().notRequired(),
            invitedGuests: yup.array<string[]>().required(),
            notificationTime: yup.date().required(),
        })
        .validate(event);

    return event;
};

const ESTIMATED_TIME_REGEX =
    /^([0-9]*y)? ?([0-9]*M)? ?([0-9]*w)? ?([0-9]*d)? ?(([0-1]?[0-9]|2[0-3])h)? ?([0-5]?[0-9]m)? ?([0-5]?[0-9]s)?$/g;
