import { Item } from '../generated/graphql';

export const isItem = (obj: object): obj is Item =>
    'id' in obj && 'title' in obj && 'description' in obj;
