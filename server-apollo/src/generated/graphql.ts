import {
    GraphQLResolveInfo,
    GraphQLScalarType,
    GraphQLScalarTypeConfig,
} from 'graphql';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = {
    [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
    [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
    [SubKey in K]: Maybe<T[SubKey]>;
};
export type RequireFields<T, K extends keyof T> = {
    [X in Exclude<keyof T, K>]?: T[X];
} & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
    ID: string;
    String: string;
    Boolean: boolean;
    Int: number;
    Float: number;
    /** Convert ISO date formatted string to Date object and otherwise */
    ISODate: any;
    TaskPriority: any;
    TaskStatus: any;
};

export type Event = Item & {
    __typename?: 'Event';
    beginningTime: Scalars['ISODate'];
    color: Scalars['String'];
    description: Scalars['String'];
    endingTime: Scalars['ISODate'];
    id: Scalars['String'];
    invitedGuests: Array<Scalars['String']>;
    location?: Maybe<Scalars['String']>;
    notificationTime: Scalars['ISODate'];
    title: Scalars['String'];
};

export type EventData = {
    beginningTime: Scalars['String'];
    color: Scalars['String'];
    description: Scalars['String'];
    endingTime: Scalars['String'];
    id: Scalars['String'];
    invitedGuests: Array<Scalars['String']>;
    location?: InputMaybe<Scalars['String']>;
    notificationTime: Scalars['String'];
    title: Scalars['String'];
};

export type EventFilters = {
    eventsForToday?: InputMaybe<Scalars['Boolean']>;
    futureEvents?: InputMaybe<Scalars['Boolean']>;
    searchFilter: Scalars['String'];
};

export type EventSubscriptionType = {
    __typename?: 'EventSubscriptionType';
    deletedId?: Maybe<Scalars['String']>;
    event?: Maybe<Event>;
    mutation: MutationType;
};

export type Item = {
    description: Scalars['String'];
    id: Scalars['String'];
    title: Scalars['String'];
};

export type Mutation = {
    __typename?: 'Mutation';
    addEvent: Event;
    addTask: Task;
    deleteEvent: Scalars['String'];
    deleteTask: Scalars['String'];
    updateEvent: Event;
    updateTask: Task;
};

export type MutationAddEventArgs = {
    data: EventData;
};

export type MutationAddTaskArgs = {
    data: TaskData;
};

export type MutationDeleteEventArgs = {
    id: Scalars['String'];
};

export type MutationDeleteTaskArgs = {
    id: Scalars['String'];
};

export type MutationUpdateEventArgs = {
    data: EventData;
};

export type MutationUpdateTaskArgs = {
    data: TaskData;
};

export enum MutationType {
    Add = 'ADD',
    Delete = 'DELETE',
    Update = 'UPDATE',
}

export type Query = {
    __typename?: 'Query';
    events?: Maybe<Array<Event>>;
    tasks?: Maybe<Array<Task>>;
};

export type QueryEventsArgs = {
    filters: EventFilters;
};

export type QueryTasksArgs = {
    filters: TaskFilters;
};

export type Subscription = {
    __typename?: 'Subscription';
    eventSubscription: EventSubscriptionType;
    taskSubscription: TaskSubscriptionType;
};

export type SubscriptionEventSubscriptionArgs = {
    filters: EventFilters;
};

export type SubscriptionTaskSubscriptionArgs = {
    filters: TaskFilters;
};

export type Task = Item & {
    __typename?: 'Task';
    description: Scalars['String'];
    estimatedTime: Scalars['String'];
    id: Scalars['String'];
    priority: Scalars['TaskPriority'];
    review?: Maybe<Scalars['String']>;
    status: Scalars['TaskStatus'];
    timeSpent?: Maybe<Scalars['String']>;
    title: Scalars['String'];
    untilDate?: Maybe<Scalars['ISODate']>;
};

export type TaskData = {
    description: Scalars['String'];
    estimatedTime: Scalars['String'];
    id: Scalars['String'];
    priority: Scalars['String'];
    review?: InputMaybe<Scalars['String']>;
    status: Scalars['String'];
    timeSpent?: InputMaybe<Scalars['String']>;
    title: Scalars['String'];
    untilDate?: InputMaybe<Scalars['String']>;
};

export type TaskFilters = {
    highPriorityTasks?: InputMaybe<Scalars['Boolean']>;
    searchFilter: Scalars['String'];
    uncompletedTasks?: InputMaybe<Scalars['Boolean']>;
};

export type TaskSubscriptionType = {
    __typename?: 'TaskSubscriptionType';
    deletedId?: Maybe<Scalars['String']>;
    mutation: MutationType;
    task?: Maybe<Task>;
};

export type WithIndex<TObject> = TObject & Record<string, any>;
export type ResolversObject<TObject> = WithIndex<TObject>;

export type ResolverTypeWrapper<T> = Promise<T> | T;

export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
    resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
    | ResolverFn<TResult, TParent, TContext, TArgs>
    | ResolverWithResolve<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
    parent: TParent,
    args: TArgs,
    context: TContext,
    info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
    parent: TParent,
    args: TArgs,
    context: TContext,
    info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
    parent: TParent,
    args: TArgs,
    context: TContext,
    info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<
    TResult,
    TKey extends string,
    TParent,
    TContext,
    TArgs
> {
    subscribe: SubscriptionSubscribeFn<
        { [key in TKey]: TResult },
        TParent,
        TContext,
        TArgs
    >;
    resolve?: SubscriptionResolveFn<
        TResult,
        { [key in TKey]: TResult },
        TContext,
        TArgs
    >;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
    subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
    resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<
    TResult,
    TKey extends string,
    TParent,
    TContext,
    TArgs
> =
    | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
    | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<
    TResult,
    TKey extends string,
    TParent = {},
    TContext = {},
    TArgs = {}
> =
    | ((
          ...args: any[]
      ) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
    | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
    parent: TParent,
    context: TContext,
    info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (
    obj: T,
    context: TContext,
    info: GraphQLResolveInfo
) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<
    TResult = {},
    TParent = {},
    TContext = {},
    TArgs = {}
> = (
    next: NextResolverFn<TResult>,
    parent: TParent,
    args: TArgs,
    context: TContext,
    info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = ResolversObject<{
    Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
    Event: ResolverTypeWrapper<Event>;
    EventData: EventData;
    EventFilters: EventFilters;
    EventSubscriptionType: ResolverTypeWrapper<EventSubscriptionType>;
    ISODate: ResolverTypeWrapper<Scalars['ISODate']>;
    Item: ResolversTypes['Event'] | ResolversTypes['Task'];
    Mutation: ResolverTypeWrapper<{}>;
    MutationType: MutationType;
    Query: ResolverTypeWrapper<{}>;
    String: ResolverTypeWrapper<Scalars['String']>;
    Subscription: ResolverTypeWrapper<{}>;
    Task: ResolverTypeWrapper<Task>;
    TaskData: TaskData;
    TaskFilters: TaskFilters;
    TaskPriority: ResolverTypeWrapper<Scalars['TaskPriority']>;
    TaskStatus: ResolverTypeWrapper<Scalars['TaskStatus']>;
    TaskSubscriptionType: ResolverTypeWrapper<TaskSubscriptionType>;
}>;

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = ResolversObject<{
    Boolean: Scalars['Boolean'];
    Event: Event;
    EventData: EventData;
    EventFilters: EventFilters;
    EventSubscriptionType: EventSubscriptionType;
    ISODate: Scalars['ISODate'];
    Item: ResolversParentTypes['Event'] | ResolversParentTypes['Task'];
    Mutation: {};
    Query: {};
    String: Scalars['String'];
    Subscription: {};
    Task: Task;
    TaskData: TaskData;
    TaskFilters: TaskFilters;
    TaskPriority: Scalars['TaskPriority'];
    TaskStatus: Scalars['TaskStatus'];
    TaskSubscriptionType: TaskSubscriptionType;
}>;

export type EventResolvers<
    ContextType = any,
    ParentType extends ResolversParentTypes['Event'] = ResolversParentTypes['Event']
> = ResolversObject<{
    beginningTime?: Resolver<
        ResolversTypes['ISODate'],
        ParentType,
        ContextType
    >;
    color?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    description?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    endingTime?: Resolver<ResolversTypes['ISODate'], ParentType, ContextType>;
    id?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    invitedGuests?: Resolver<
        Array<ResolversTypes['String']>,
        ParentType,
        ContextType
    >;
    location?: Resolver<
        Maybe<ResolversTypes['String']>,
        ParentType,
        ContextType
    >;
    notificationTime?: Resolver<
        ResolversTypes['ISODate'],
        ParentType,
        ContextType
    >;
    title?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type EventSubscriptionTypeResolvers<
    ContextType = any,
    ParentType extends ResolversParentTypes['EventSubscriptionType'] = ResolversParentTypes['EventSubscriptionType']
> = ResolversObject<{
    deletedId?: Resolver<
        Maybe<ResolversTypes['String']>,
        ParentType,
        ContextType
    >;
    event?: Resolver<Maybe<ResolversTypes['Event']>, ParentType, ContextType>;
    mutation?: Resolver<
        ResolversTypes['MutationType'],
        ParentType,
        ContextType
    >;
    __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export interface IsoDateScalarConfig
    extends GraphQLScalarTypeConfig<ResolversTypes['ISODate'], any> {
    name: 'ISODate';
}

export type ItemResolvers<
    ContextType = any,
    ParentType extends ResolversParentTypes['Item'] = ResolversParentTypes['Item']
> = ResolversObject<{
    __resolveType: TypeResolveFn<'Event' | 'Task', ParentType, ContextType>;
    description?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    id?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    title?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
}>;

export type MutationResolvers<
    ContextType = any,
    ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']
> = ResolversObject<{
    addEvent?: Resolver<
        ResolversTypes['Event'],
        ParentType,
        ContextType,
        RequireFields<MutationAddEventArgs, 'data'>
    >;
    addTask?: Resolver<
        ResolversTypes['Task'],
        ParentType,
        ContextType,
        RequireFields<MutationAddTaskArgs, 'data'>
    >;
    deleteEvent?: Resolver<
        ResolversTypes['String'],
        ParentType,
        ContextType,
        RequireFields<MutationDeleteEventArgs, 'id'>
    >;
    deleteTask?: Resolver<
        ResolversTypes['String'],
        ParentType,
        ContextType,
        RequireFields<MutationDeleteTaskArgs, 'id'>
    >;
    updateEvent?: Resolver<
        ResolversTypes['Event'],
        ParentType,
        ContextType,
        RequireFields<MutationUpdateEventArgs, 'data'>
    >;
    updateTask?: Resolver<
        ResolversTypes['Task'],
        ParentType,
        ContextType,
        RequireFields<MutationUpdateTaskArgs, 'data'>
    >;
}>;

export type QueryResolvers<
    ContextType = any,
    ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']
> = ResolversObject<{
    events?: Resolver<
        Maybe<Array<ResolversTypes['Event']>>,
        ParentType,
        ContextType,
        RequireFields<QueryEventsArgs, 'filters'>
    >;
    tasks?: Resolver<
        Maybe<Array<ResolversTypes['Task']>>,
        ParentType,
        ContextType,
        RequireFields<QueryTasksArgs, 'filters'>
    >;
}>;

export type SubscriptionResolvers<
    ContextType = any,
    ParentType extends ResolversParentTypes['Subscription'] = ResolversParentTypes['Subscription']
> = ResolversObject<{
    eventSubscription?: SubscriptionResolver<
        ResolversTypes['EventSubscriptionType'],
        'eventSubscription',
        ParentType,
        ContextType,
        RequireFields<SubscriptionEventSubscriptionArgs, 'filters'>
    >;
    taskSubscription?: SubscriptionResolver<
        ResolversTypes['TaskSubscriptionType'],
        'taskSubscription',
        ParentType,
        ContextType,
        RequireFields<SubscriptionTaskSubscriptionArgs, 'filters'>
    >;
}>;

export type TaskResolvers<
    ContextType = any,
    ParentType extends ResolversParentTypes['Task'] = ResolversParentTypes['Task']
> = ResolversObject<{
    description?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    estimatedTime?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    id?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    priority?: Resolver<
        ResolversTypes['TaskPriority'],
        ParentType,
        ContextType
    >;
    review?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
    status?: Resolver<ResolversTypes['TaskStatus'], ParentType, ContextType>;
    timeSpent?: Resolver<
        Maybe<ResolversTypes['String']>,
        ParentType,
        ContextType
    >;
    title?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    untilDate?: Resolver<
        Maybe<ResolversTypes['ISODate']>,
        ParentType,
        ContextType
    >;
    __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export interface TaskPriorityScalarConfig
    extends GraphQLScalarTypeConfig<ResolversTypes['TaskPriority'], any> {
    name: 'TaskPriority';
}

export interface TaskStatusScalarConfig
    extends GraphQLScalarTypeConfig<ResolversTypes['TaskStatus'], any> {
    name: 'TaskStatus';
}

export type TaskSubscriptionTypeResolvers<
    ContextType = any,
    ParentType extends ResolversParentTypes['TaskSubscriptionType'] = ResolversParentTypes['TaskSubscriptionType']
> = ResolversObject<{
    deletedId?: Resolver<
        Maybe<ResolversTypes['String']>,
        ParentType,
        ContextType
    >;
    mutation?: Resolver<
        ResolversTypes['MutationType'],
        ParentType,
        ContextType
    >;
    task?: Resolver<Maybe<ResolversTypes['Task']>, ParentType, ContextType>;
    __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type Resolvers<ContextType = any> = ResolversObject<{
    Event?: EventResolvers<ContextType>;
    EventSubscriptionType?: EventSubscriptionTypeResolvers<ContextType>;
    ISODate?: GraphQLScalarType;
    Item?: ItemResolvers<ContextType>;
    Mutation?: MutationResolvers<ContextType>;
    Query?: QueryResolvers<ContextType>;
    Subscription?: SubscriptionResolvers<ContextType>;
    Task?: TaskResolvers<ContextType>;
    TaskPriority?: GraphQLScalarType;
    TaskStatus?: GraphQLScalarType;
    TaskSubscriptionType?: TaskSubscriptionTypeResolvers<ContextType>;
}>;
