import express, { Express, Request, Response } from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import dotenv from 'dotenv';
import { getDB } from './src/database';
import { exit } from 'process';
import TasksRouter from './src/routers/TasksRouter';
import EventsRouter from './src/routers/EventsRouter';
const cors = require('cors');


dotenv.config();

const PORT = process.env.PORT || 3001;
const app: Express = express();

app.use(helmet());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/tasks', TasksRouter);
app.use('/events', EventsRouter);

app.listen(PORT, async () => {
  try {
    await getDB();
    console.log(`Running on ${PORT} ⚡`)
  } catch (e) {
    console.error('Could not initialize DB', e);
    exit(0);
  }
});