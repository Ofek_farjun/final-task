import Item from "./ItemInterface";

export default interface Event extends Item {
    beginningTime: Date,
    endingTime: Date,
    color: string,
    location?: string,
    invitedGuests: string[],
    notificationTime: Date,
}

export interface EventDTO extends Item {
    beginningTime: string,
    endingTime: string,
    color: string,
    location?: string,
    invitedGuests: string[],
    notificationTime: string,
}

export const convertDTOToEvent = (dto: EventDTO): Event => ({
    id: dto.id,
    title: dto.title,
    description: dto.description,
    beginningTime: new Date(dto.beginningTime),
    endingTime: new Date(dto.endingTime),
    color: dto.color,
    location: dto.location,
    invitedGuests: dto.invitedGuests,
    notificationTime: new Date(dto.notificationTime),
});