import Item from "./ItemInterface";

export default interface Task extends Item {
    estimatedTime: string,
    status: Status,
    priority: Priority,
    review?: string,
    timeSpent?: string,
    untilDate?: Date,
}

export interface TaskDTO extends Item {
    estimatedTime: string,
    status: string,
    priority: string,
    review?: string,
    timeSpent?: string,
    untilDate?: string,
}

export const convertDTOToTask = (dto: TaskDTO): Task => {
    return {
        id: dto.id,
        title: dto.title,
        description: dto.description,
        estimatedTime: dto.estimatedTime,
        status: dto.status as Status,
        priority: dto.priority as Priority,
        review: dto.review,
        timeSpent: dto.timeSpent,
        untilDate: dto.untilDate ? new Date(dto.untilDate) : undefined,
    }
}

export enum Status {
    OPEN = 'Open',
    IN_PROGRESS = 'In Progress',
    CLOSE = 'Close'
}

export enum Priority {
    LOW = 'Low',
    MEDIUM = 'Medium',
    HIGH = 'High'
}

export const isTask = (item: (Item | null)) => item && 'priority' in item;