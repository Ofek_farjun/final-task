import { Router } from "express";
import { addTask, deleteTask, getTasks, updateTask } from "../services/TasksService";

const router = Router();

router.get('/', getTasks);
router.post('/', addTask);
router.put('/', updateTask);
router.delete('/:id', deleteTask);

export default router;