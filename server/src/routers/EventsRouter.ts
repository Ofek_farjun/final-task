import { Router } from "express";
import { addEvent, deleteEvent, getEvents, updateEvent } from "../services/EventsService";

const router = Router();

router.get('/', getEvents);
router.post('/', addEvent);
router.put('/', updateEvent);
router.delete('/:id', deleteEvent);

export default router;