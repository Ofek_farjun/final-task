import express from "express";
import { getDB } from "../database";
import { validateBodyAsEvent } from "../utils/ValidationUtils";

export const getEvents = async (req: express.Request, res: express.Response) => {
    try {
        const db = await getDB();
        res.send(await db.events.getEvents());
    } catch (error) {
        res.status(500).send((error as Error).message || error);
    }
}

export const deleteEvent = async (req: express.Request, res: express.Response) => {
    try {
        const db = await getDB();
        const deletedId = await db.events.deleteEvent(req.params.id);
        res.send(deletedId);
    } catch (error) {
        res.status(500).send((error as Error).message || error);
    }
}

export const updateEvent = async (req: express.Request, res: express.Response) => {
    try {
        const db = await getDB();
        const updatedEventResult = await db.events.updateEvent(await validateBodyAsEvent(req.body));
        res.send(updatedEventResult);
    } catch (error) {
        res.status(500).send((error as Error).message || error);
    }
}

export const addEvent = async (req: express.Request, res: express.Response) => {
    try {
        const db = await getDB();
        const newEventResult = await db.events.addEvent(await validateBodyAsEvent(req.body));
        res.send(newEventResult);
    } catch (error) {
        res.status(500).send((error as Error).message || error);
    }
}