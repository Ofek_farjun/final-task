import express from "express";
import { getDB } from "../database";
import { validateBodyAsTask } from "../utils/ValidationUtils";

export const getTasks = async (req: express.Request, res: express.Response) => {
    try {
        const db = await getDB();
        res.send(await db.tasks.getTasks());
    } catch (error) {
        res.status(500).send((error as Error).message || error);
    }
}

export const deleteTask = async (req: express.Request, res: express.Response) => {
    try {
        const db = await getDB();
        const deleteId = await db.tasks.deleteTask(req.params.id);
        res.send(deleteId);
    } catch (error) {
        res.status(500).send((error as Error).message || error);
    }
}

export const updateTask = async (req: express.Request, res: express.Response) => {
    try {
        const db = await getDB();
        const updatedTaskResult = await db.tasks.updateTask(await validateBodyAsTask(req.body));
        res.send(updatedTaskResult);
    } catch (error) {
        res.status(500).send((error as Error).message || error);
    }
}

export const addTask = async (req: express.Request, res: express.Response) => {
    try {
        const db = await getDB();
        const newTaskResult = await db.tasks.addTask(await validateBodyAsTask(req.body));
        res.send(newTaskResult);
    } catch (error) {
        res.status(500).send((error as Error).message || error);
    }
}