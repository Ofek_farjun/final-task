import { globalValues } from "..";
import Event from "../../interfaces/EventInterface";
import { doesIdExists } from "../../utils/ValidationUtils";

export class EventsRepository {

    async getEvents(): Promise<Event[]> {
        return globalValues.events;
    }

    async addEvent(event: Event) {
        if (doesIdExists(globalValues.events, event.id))
            throw new Error('Id already exists');

        globalValues.events = globalValues.events.concat([event]);
        
        return globalValues.events.find(currEvent => currEvent.id, event.id);
    }

    async updateEvent(event: Event) {
        if (!doesIdExists(globalValues.events, event.id))
            throw new Error('Id does not exist');

        Object.assign(globalValues.events.find(currEvent => event.id === currEvent.id), event);
        
        return globalValues.events.find(currEvent => currEvent.id, event.id);
    }

    async deleteEvent(id: string) {
        if (!doesIdExists(globalValues.events, id))
            throw new Error('Id does not exist');

        globalValues.events = globalValues.events.filter(event => event.id !== id);

        return id;
    }
}