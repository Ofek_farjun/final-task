import { timingSafeEqual } from "crypto";
import { globalValues } from "..";
import Task from "../../interfaces/TaskInterface";
import { doesIdExists } from "../../utils/ValidationUtils";

export class TasksRepository {

    async getTasks(): Promise<Task[]> {
        return globalValues.tasks;
    }

    async addTask(task: Task) {
        if (doesIdExists(globalValues.tasks, task.id))
            throw new Error('Id already exists');

        globalValues.tasks = globalValues.tasks.concat([task]);
        return globalValues.tasks.find(currTask => currTask.id === task.id);
    }

    async updateTask(task: Task) {
        if (!doesIdExists(globalValues.tasks, task.id))
            throw new Error('Id does not exist');

        Object.assign(globalValues.tasks.find(currTask => task.id === currTask.id), task);
        return globalValues.tasks.find(currTask => currTask.id === task.id);
    }

    async deleteTask(id: string) {
        if (!doesIdExists(globalValues.tasks, id))
            throw new Error('Id does not exist');

        globalValues.tasks = globalValues.tasks.filter(task => task.id !== id);
        return id;
    }
}