import Task, { convertDTOToTask } from "../interfaces/TaskInterface";
import mockData from '../../assets/mock.json';
import Event, { convertDTOToEvent } from "../interfaces/EventInterface";
import { TasksRepository } from "./repositories/TasksRepository";
import { EventsRepository } from "./repositories/EventsRepository";

const tasks: Task[] = [];
const events: Event[] = [];

export const globalValues = {
    tasks,
    events,
}

const db = {
    isInitialized: false,
    tasks: new TasksRepository(),
    events: new EventsRepository(),
}

export const getDB = async () => {
    if(db.isInitialized) return db;

    globalValues.tasks = mockData.tasks.map(convertDTOToTask);
    globalValues.events = mockData.events.map(convertDTOToEvent);
    db.isInitialized = true;
    console.log('DB initialized successfully');
   
    return db;
}